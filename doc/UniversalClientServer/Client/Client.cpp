#include "Client.h"


Client::Client()
{
    this->port=htons(5555);
};

Client::~Client()
{

};

Client::Client(dv::SmartDevices device, uint16_t port)
{
    this->devices=device;
    this->port=htons(port);
}



void Client:: startClient()
{
    if ((serverSocket = socket(AF_INET, SOCK_STREAM, 0)) == -1) 
    {
        std::cout << "Socket creation failed with error: "  << std::endl;
        exit(ERROR_CREATE_SOCKET);
    }
    
    serverAddr.sin_addr.s_addr = inet_addr("127.0.0.1"); 
    serverAddr.sin_family = AF_INET;
    serverAddr.sin_port = port;
    if (connect(serverSocket, (sockaddr*)&serverAddr, sizeof(serverAddr)) == -1) 
    {
        std::cout << "Server connection failed with error: "  << std::endl;
        exit(ERROR_CONNECTION);
    }
    
    std::cout << "Connected to server!" << std::endl;
    clientReceive();
}

void Client::clientReceive()
{
    uint32_t dataLength;
    std::string receivedString;
    std::vector<char> rcvBuf;  
    while (true) 
    {  
    
        recv(serverSocket,&dataLength,sizeof(uint32_t),0); // Receive the message length
        dataLength = ntohl(dataLength ); // Ensure host system byte order
        rcvBuf.resize(dataLength,0x00);

        recv(serverSocket,&(rcvBuf[0]),dataLength,0); // Receive the string data
        receivedString.assign(&(rcvBuf[0]),rcvBuf.size()); 

        std::cout <<"Server: "<< receivedString << std::endl;

        handleReceivedString(receivedString);

        rcvBuf.clear();
        receivedString.clear();

    }
};

void Client::handleReceivedString(std::string recievedString)
{
	std::vector<std::string> splitted;
    boost::split(splitted,recievedString,boost::is_any_of(" "));
    if(splitted[0]=="GET")
    {
        if(splitted[1] == "devices")
        {
            std::string devicesString;
            nlohmann::json devicesJson = devices;
            devicesString=devicesJson.dump();
            clientSend(devicesString);
        }
    }
    else clientSend("123456789RROR!");
};

void Client::clientSend(std::string sendString)
{
    uint32_t dataLength = htonl(sendString.size()); // Ensure network byte order 
                                                // when sending the data length

    if( (send(serverSocket,&dataLength ,sizeof(uint32_t) ,0) == -1) ||
        (send(serverSocket,sendString.c_str(),sendString.size(),0) == -1) )
        std::cout << "send failed with error: "  << std::endl;
};