#pragma once
#include "../include/include.h"
#include "Devices.h"


typedef struct socketArg_tag {
    int socket;
} socketArg_t;

class Client
{
    private:
    int serverSocket;
    uint16_t port;
    sockaddr_in serverAddr;
    void clientReceive();
    void clientSend(std::string);
    socketArg_t arg;
    dv::SmartDevices devices;
    void handleReceivedString(std::string);
    public:
    Client();
    Client(dv::SmartDevices devices, uint16_t port);
    ~Client();
    void startClient();

};