//  To parse this JSON data, first install
//
//      Boost     http://www.boost.org
//      json.hpp  https://github.com/nlohmann/json
//
//  Then include this file, and then do
//
//     SmartDevices data = nlohmann::json::parse(jsonString);

#pragma once

#include "../include/include.h"
//  To parse this JSON data, first install
//
//      Boost     http://www.boost.org
//      json.hpp  https://github.com/nlohmann/json
//
//  Then include this file, and then do
//
//     SmartDevices data = nlohmann::json::parse(jsonString);

#pragma once

#include <boost/variant.hpp>
#include <nlohmann/json.hpp>

#include <boost/optional.hpp>
#include <stdexcept>
#include <regex>

#ifndef NLOHMANN_OPT_HELPER
#define NLOHMANN_OPT_HELPER
namespace nlohmann {
    template <typename T>
    struct adl_serializer<std::shared_ptr<T>> {
        static void to_json(json & j, const std::shared_ptr<T> & opt) {
            if (!opt) j = nullptr; else j = *opt;
        }

        static std::shared_ptr<T> from_json(const json & j) {
            if (j.is_null()) return std::unique_ptr<T>(); else return std::unique_ptr<T>(new T(j.get<T>()));
        }
    };
}
#endif

namespace dv {
    using nlohmann::json;

    inline json get_untyped(const json & j, const char * property) {
        if (j.find(property) != j.end()) {
            return j.at(property).get<json>();
        }
        return json();
    }

    inline json get_untyped(const json & j, std::string property) {
        return get_untyped(j, property.data());
    }

    template <typename T>
    inline std::shared_ptr<T> get_optional(const json & j, const char * property) {
        if (j.find(property) != j.end()) {
            return j.at(property).get<std::shared_ptr<T>>();
        }
        return std::shared_ptr<T>();
    }

    template <typename T>
    inline std::shared_ptr<T> get_optional(const json & j, std::string property) {
        return get_optional<T>(j, property.data());
    }

    class Scene {
        public:
        Scene() = default;
        virtual ~Scene() = default;

        private:
        std::string id;

        public:
        const std::string & get_id() const { return id; }
        std::string & get_mutable_id() { return id; }
        void set_id(const std::string & value) { this->id = value; }
    };

    class ColorScene {
        public:
        ColorScene() = default;
        virtual ~ColorScene() = default;

        private:
        std::vector<Scene> scenes;

        public:
        const std::vector<Scene> & get_scenes() const { return scenes; }
        std::vector<Scene> & get_mutable_scenes() { return scenes; }
        void set_scenes(const std::vector<Scene> & value) { this->scenes = value; }
    };

    class Range {
        public:
        Range() = default;
        virtual ~Range() = default;

        private:
        int64_t min;
        int64_t max;
        std::shared_ptr<int64_t> precision;

        public:
        const int64_t & get_min() const { return min; }
        int64_t & get_mutable_min() { return min; }
        void set_min(const int64_t & value) { this->min = value; }

        const int64_t & get_max() const { return max; }
        int64_t & get_mutable_max() { return max; }
        void set_max(const int64_t & value) { this->max = value; }

        std::shared_ptr<int64_t> get_precision() const { return precision; }
        void set_precision(std::shared_ptr<int64_t> value) { this->precision = value; }
    };

    class Parameters {
        public:
        Parameters() = default;
        virtual ~Parameters() = default;

        private:
        std::shared_ptr<std::string> instance;
        std::shared_ptr<std::string> unit;
        std::shared_ptr<Range> range;
        std::shared_ptr<std::string> color_model;
        std::shared_ptr<Range> temperature_k;
        std::shared_ptr<bool> random_access;
        std::shared_ptr<bool> split;
        std::shared_ptr<ColorScene> color_scene;

        public:
        std::shared_ptr<std::string> get_instance() const { return instance; }
        void set_instance(std::shared_ptr<std::string> value) { this->instance = value; }

        std::shared_ptr<std::string> get_unit() const { return unit; }
        void set_unit(std::shared_ptr<std::string> value) { this->unit = value; }

        std::shared_ptr<Range> get_range() const { return range; }
        void set_range(std::shared_ptr<Range> value) { this->range = value; }

        std::shared_ptr<std::string> get_color_model() const { return color_model; }
        void set_color_model(std::shared_ptr<std::string> value) { this->color_model = value; }

        std::shared_ptr<Range> get_temperature_k() const { return temperature_k; }
        void set_temperature_k(std::shared_ptr<Range> value) { this->temperature_k = value; }

        std::shared_ptr<bool> get_random_access() const { return random_access; }
        void set_random_access(std::shared_ptr<bool> value) { this->random_access = value; }

        std::shared_ptr<bool> get_split() const { return split; }
        void set_split(std::shared_ptr<bool> value) { this->split = value; }

        std::shared_ptr<ColorScene> get_color_scene() const { return color_scene; }
        void set_color_scene(std::shared_ptr<ColorScene> value) { this->color_scene = value; }
    };

    class Capability {
        public:
        Capability() = default;
        virtual ~Capability() = default;

        private:
        std::string type;
        std::shared_ptr<bool> retrievable;
        std::shared_ptr<Parameters> parameters;
        std::shared_ptr<bool> reportable;

        public:
        const std::string & get_type() const { return type; }
        std::string & get_mutable_type() { return type; }
        void set_type(const std::string & value) { this->type = value; }

        std::shared_ptr<bool> get_retrievable() const { return retrievable; }
        void set_retrievable(std::shared_ptr<bool> value) { this->retrievable = value; }

        std::shared_ptr<Parameters> get_parameters() const { return parameters; }
        void set_parameters(std::shared_ptr<Parameters> value) { this->parameters = value; }

        std::shared_ptr<bool> get_reportable() const { return reportable; }
        void set_reportable(std::shared_ptr<bool> value) { this->reportable = value; }
    };

    class Quuz {
        public:
        Quuz() = default;
        virtual ~Quuz() = default;

        private:
        std::vector<nlohmann::json> corge;

        public:
        const std::vector<nlohmann::json> & get_corge() const { return corge; }
        std::vector<nlohmann::json> & get_mutable_corge() { return corge; }
        void set_corge(const std::vector<nlohmann::json> & value) { this->corge = value; }
    };

    class Quux {
        public:
        Quux() = default;
        virtual ~Quux() = default;

        private:
        Quuz quuz;

        public:
        const Quuz & get_quuz() const { return quuz; }
        Quuz & get_mutable_quuz() { return quuz; }
        void set_quuz(const Quuz & value) { this->quuz = value; }
    };

    using Qux = boost::variant<bool, int64_t, std::string>;

    class CustomData {
        public:
        CustomData() = default;
        virtual ~CustomData() = default;

        private:
        int64_t foo;
        std::string bar;
        bool baz;
        std::vector<Qux> qux;
        Quux quux;

        public:
        const int64_t & get_foo() const { return foo; }
        int64_t & get_mutable_foo() { return foo; }
        void set_foo(const int64_t & value) { this->foo = value; }

        const std::string & get_bar() const { return bar; }
        std::string & get_mutable_bar() { return bar; }
        void set_bar(const std::string & value) { this->bar = value; }

        const bool & get_baz() const { return baz; }
        bool & get_mutable_baz() { return baz; }
        void set_baz(const bool & value) { this->baz = value; }

        const std::vector<Qux> & get_qux() const { return qux; }
        std::vector<Qux> & get_mutable_qux() { return qux; }
        void set_qux(const std::vector<Qux> & value) { this->qux = value; }

        const Quux & get_quux() const { return quux; }
        Quux & get_mutable_quux() { return quux; }
        void set_quux(const Quux & value) { this->quux = value; }
    };

    class DeviceInfo {
        public:
        DeviceInfo() = default;
        virtual ~DeviceInfo() = default;

        private:
        std::string manufacturer;
        std::string model;
        std::shared_ptr<std::string> hw_version;
        std::shared_ptr<std::string> sw_version;

        public:
        const std::string & get_manufacturer() const { return manufacturer; }
        std::string & get_mutable_manufacturer() { return manufacturer; }
        void set_manufacturer(const std::string & value) { this->manufacturer = value; }

        const std::string & get_model() const { return model; }
        std::string & get_mutable_model() { return model; }
        void set_model(const std::string & value) { this->model = value; }

        std::shared_ptr<std::string> get_hw_version() const { return hw_version; }
        void set_hw_version(std::shared_ptr<std::string> value) { this->hw_version = value; }

        std::shared_ptr<std::string> get_sw_version() const { return sw_version; }
        void set_sw_version(std::shared_ptr<std::string> value) { this->sw_version = value; }
    };

    class Device {
        public:
        Device() = default;
        virtual ~Device() = default;

        private:
        std::string id;
        std::string name;
        std::string description;
        std::string room;
        std::string type;
        std::shared_ptr<CustomData> custom_data;
        std::vector<Capability> capabilities;
        DeviceInfo device_info;

        public:
        const std::string & get_id() const { return id; }
        std::string & get_mutable_id() { return id; }
        void set_id(const std::string & value) { this->id = value; }

        const std::string & get_name() const { return name; }
        std::string & get_mutable_name() { return name; }
        void set_name(const std::string & value) { this->name = value; }

        const std::string & get_description() const { return description; }
        std::string & get_mutable_description() { return description; }
        void set_description(const std::string & value) { this->description = value; }

        const std::string & get_room() const { return room; }
        std::string & get_mutable_room() { return room; }
        void set_room(const std::string & value) { this->room = value; }

        const std::string & get_type() const { return type; }
        std::string & get_mutable_type() { return type; }
        void set_type(const std::string & value) { this->type = value; }

        std::shared_ptr<CustomData> get_custom_data() const { return custom_data; }
        void set_custom_data(std::shared_ptr<CustomData> value) { this->custom_data = value; }

        const std::vector<Capability> & get_capabilities() const { return capabilities; }
        std::vector<Capability> & get_mutable_capabilities() { return capabilities; }
        void set_capabilities(const std::vector<Capability> & value) { this->capabilities = value; }

        const DeviceInfo & get_device_info() const { return device_info; }
        DeviceInfo & get_mutable_device_info() { return device_info; }
        void set_device_info(const DeviceInfo & value) { this->device_info = value; }
    };

    class SmartDevices {
        public:
        SmartDevices() = default;
        virtual ~SmartDevices() = default;

        private:
        std::vector<Device> devices;

        public:
        const std::vector<Device> & get_devices() const { return devices; }
        std::vector<Device> & get_mutable_devices() { return devices; }
        void set_devices(const std::vector<Device> & value) { this->devices = value; }
    };
}

namespace nlohmann {
    void from_json(const json & j, dv::Scene & x);
    void to_json(json & j, const dv::Scene & x);

    void from_json(const json & j, dv::ColorScene & x);
    void to_json(json & j, const dv::ColorScene & x);

    void from_json(const json & j, dv::Range & x);
    void to_json(json & j, const dv::Range & x);

    void from_json(const json & j, dv::Parameters & x);
    void to_json(json & j, const dv::Parameters & x);

    void from_json(const json & j, dv::Capability & x);
    void to_json(json & j, const dv::Capability & x);

    void from_json(const json & j, dv::Quuz & x);
    void to_json(json & j, const dv::Quuz & x);

    void from_json(const json & j, dv::Quux & x);
    void to_json(json & j, const dv::Quux & x);

    void from_json(const json & j, dv::CustomData & x);
    void to_json(json & j, const dv::CustomData & x);

    void from_json(const json & j, dv::DeviceInfo & x);
    void to_json(json & j, const dv::DeviceInfo & x);

    void from_json(const json & j, dv::Device & x);
    void to_json(json & j, const dv::Device & x);

    void from_json(const json & j, dv::SmartDevices & x);
    void to_json(json & j, const dv::SmartDevices & x);

    void from_json(const json & j, boost::variant<bool, int64_t, std::string> & x);
    void to_json(json & j, const boost::variant<bool, int64_t, std::string> & x);

    inline void from_json(const json & j, dv::Scene& x) {
        x.set_id(j.at("id").get<std::string>());
    }

    inline void to_json(json & j, const dv::Scene & x) {
        j = json::object();
        j["id"] = x.get_id();
    }

    inline void from_json(const json & j, dv::ColorScene& x) {
        x.set_scenes(j.at("scenes").get<std::vector<dv::Scene>>());
    }

    inline void to_json(json & j, const dv::ColorScene & x) {
        j = json::object();
        j["scenes"] = x.get_scenes();
    }

    inline void from_json(const json & j, dv::Range& x) {
        x.set_min(j.at("min").get<int64_t>());
        x.set_max(j.at("max").get<int64_t>());
        x.set_precision(dv::get_optional<int64_t>(j, "precision"));
    }

    inline void to_json(json & j, const dv::Range & x) {
        j = json::object();
        j["min"] = x.get_min();
        j["max"] = x.get_max();
        j["precision"] = x.get_precision();
    }

    inline void from_json(const json & j, dv::Parameters& x) {
        x.set_instance(dv::get_optional<std::string>(j, "instance"));
        x.set_unit(dv::get_optional<std::string>(j, "unit"));
        x.set_range(dv::get_optional<dv::Range>(j, "range"));
        x.set_color_model(dv::get_optional<std::string>(j, "color_model"));
        x.set_temperature_k(dv::get_optional<dv::Range>(j, "temperature_k"));
        x.set_random_access(dv::get_optional<bool>(j, "random_access"));
        x.set_split(dv::get_optional<bool>(j, "split"));
        x.set_color_scene(dv::get_optional<dv::ColorScene>(j, "color_scene"));
    }

    inline void to_json(json & j, const dv::Parameters & x) {
        j = json::object();
        j["instance"] = x.get_instance();
        j["unit"] = x.get_unit();
        j["range"] = x.get_range();
        j["color_model"] = x.get_color_model();
        j["temperature_k"] = x.get_temperature_k();
        j["random_access"] = x.get_random_access();
        j["split"] = x.get_split();
        j["color_scene"] = x.get_color_scene();
    }

    inline void from_json(const json & j, dv::Capability& x) {
        x.set_type(j.at("type").get<std::string>());
        x.set_retrievable(dv::get_optional<bool>(j, "retrievable"));
        x.set_parameters(dv::get_optional<dv::Parameters>(j, "parameters"));
        x.set_reportable(dv::get_optional<bool>(j, "reportable"));
    }

    inline void to_json(json & j, const dv::Capability & x) {
        j = json::object();
        j["type"] = x.get_type();
        j["retrievable"] = x.get_retrievable();
        j["parameters"] = x.get_parameters();
        j["reportable"] = x.get_reportable();
    }

    inline void from_json(const json & j, dv::Quuz& x) {
        x.set_corge(j.at("corge").get<std::vector<json>>());
    }

    inline void to_json(json & j, const dv::Quuz & x) {
        j = json::object();
        j["corge"] = x.get_corge();
    }

    inline void from_json(const json & j, dv::Quux& x) {
        x.set_quuz(j.at("quuz").get<dv::Quuz>());
    }

    inline void to_json(json & j, const dv::Quux & x) {
        j = json::object();
        j["quuz"] = x.get_quuz();
    }

    inline void from_json(const json & j, dv::CustomData& x) {
        x.set_foo(j.at("foo").get<int64_t>());
        x.set_bar(j.at("bar").get<std::string>());
        x.set_baz(j.at("baz").get<bool>());
        x.set_qux(j.at("qux").get<std::vector<dv::Qux>>());
        x.set_quux(j.at("quux").get<dv::Quux>());
    }

    inline void to_json(json & j, const dv::CustomData & x) {
        j = json::object();
        j["foo"] = x.get_foo();
        j["bar"] = x.get_bar();
        j["baz"] = x.get_baz();
        j["qux"] = x.get_qux();
        j["quux"] = x.get_quux();
    }

    inline void from_json(const json & j, dv::DeviceInfo& x) {
        x.set_manufacturer(j.at("manufacturer").get<std::string>());
        x.set_model(j.at("model").get<std::string>());
        x.set_hw_version(dv::get_optional<std::string>(j, "hw_version"));
        x.set_sw_version(dv::get_optional<std::string>(j, "sw_version"));
    }

    inline void to_json(json & j, const dv::DeviceInfo & x) {
        j = json::object();
        j["manufacturer"] = x.get_manufacturer();
        j["model"] = x.get_model();
        j["hw_version"] = x.get_hw_version();
        j["sw_version"] = x.get_sw_version();
    }

    inline void from_json(const json & j, dv::Device& x) {
        x.set_id(j.at("id").get<std::string>());
        x.set_name(j.at("name").get<std::string>());
        x.set_description(j.at("description").get<std::string>());
        x.set_room(j.at("room").get<std::string>());
        x.set_type(j.at("type").get<std::string>());
        x.set_custom_data(dv::get_optional<dv::CustomData>(j, "custom_data"));
        x.set_capabilities(j.at("capabilities").get<std::vector<dv::Capability>>());
        x.set_device_info(j.at("device_info").get<dv::DeviceInfo>());
    }

    inline void to_json(json & j, const dv::Device & x) {
        j = json::object();
        j["id"] = x.get_id();
        j["name"] = x.get_name();
        j["description"] = x.get_description();
        j["room"] = x.get_room();
        j["type"] = x.get_type();
        j["custom_data"] = x.get_custom_data();
        j["capabilities"] = x.get_capabilities();
        j["device_info"] = x.get_device_info();
    }

    inline void from_json(const json & j, dv::SmartDevices& x) {
        x.set_devices(j.at("devices").get<std::vector<dv::Device>>());
    }

    inline void to_json(json & j, const dv::SmartDevices & x) {
        j = json::object();
        j["devices"] = x.get_devices();
    }
    inline void from_json(const json & j, boost::variant<bool, int64_t, std::string> & x) {
        if (j.is_boolean())
            x = j.get<bool>();
        else if (j.is_number_integer())
            x = j.get<int64_t>();
        else if (j.is_string())
            x = j.get<std::string>();
        else throw "Could not deserialize";
    }

    inline void to_json(json & j, const boost::variant<bool, int64_t, std::string> & x) {
        switch (x.which()) {
            case 0:
                j = boost::get<bool>(x);
                break;
            case 1:
                j = boost::get<int64_t>(x);
                break;
            case 2:
                j = boost::get<std::string>(x);
                break;
            default: throw "Input JSON does not conform to schema";
        }
    }
}
