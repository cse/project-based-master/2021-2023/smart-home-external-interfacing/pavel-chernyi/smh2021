#include "Client.h"


int main() 
{
    std::ifstream devicesFile("Devices.json");
    nlohmann::json devicesJson;
    devicesFile>>devicesJson;
    dv::SmartDevices devicesStruct = devicesJson;

    // std::string devicesString;
    // devicesJson = devicesStruct;
    // devicesString=devicesJson.dump();
    // std::cout <<"Server: "<< devicesString << std::endl;

    Client client(devicesStruct, 5555);
    client.startClient();
    return 0;
}