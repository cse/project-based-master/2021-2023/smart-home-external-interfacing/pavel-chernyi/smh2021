#include "Server.h"



Server::Server(uint16_t port)
{
    this->serverPort=htons(port);
    this->serverSocket=-1;
    this->clientSocket=-1;;

};

Server::~Server()
{
    closeServer();
    this->serverSocket=-1;
    this->clientSocket=-1;
};


int Server::startServer()
{
    if ((serverSocket = socket(AF_INET, SOCK_STREAM, 0)) == -1 ) 
    {
        std::cout << "Socket creation failed with error:"  << std::endl;
        return(ERROR_CREATE_SOCKET);
    }
    
    int enable = 1;
    if (setsockopt(serverSocket, SOL_SOCKET, SO_REUSEADDR, &enable, sizeof(int)) != SUCCESS)
        std::cout << "SO_REUSEADDR failed with error:"  << std::endl;

    serverAddr.sin_addr.s_addr = INADDR_ANY;
    serverAddr.sin_family = AF_INET;
    serverAddr.sin_port = getServerPort();
    if (bind(serverSocket, (sockaddr*)&serverAddr, sizeof(serverAddr)) != SUCCESS) 
    {
        std::cout << "Bind function failed with error: "  << std::endl;
        return(ERROR_BIND_SOCKET);
    }
    if (listen(serverSocket, 0) != SUCCESS) 
    {
        std::cout << "Listen function failed with error:"  << std::endl;
        return(ERROR_LISTEN_SOCKET);
    }
    return SUCCESS;
};

int Server::acceptClient()
{
    socklen_t clientAddrSize = sizeof(clientAddr);
    std::cout<<"Waiting for the client."<<std::endl; 
    if( (clientSocket= accept(getServerSocket(), (sockaddr*)&clientAddr, &clientAddrSize)) == -1)
    {
        std::cout << "Clien accept failed with error:"  << std::endl;
        return(ERROR_ACCEPT_CLIENT);
    }
    arg.socket = getClientSocket();
    return SUCCESS;
};


int Server::closeServer()
{
    if(clientSocket != -1)
        if (close(clientSocket) == -1) 
        {
            std::cout << "Close client socket failed with error." << std::endl;
            return ERROR_CLOSE_CLIENT;
        } 
    if(serverSocket != -1)
        if (close(serverSocket) == -1) 
        {
            std::cout << "Close server socket failed with error." << std::endl;
            return ERROR_CLOSE_SERVER;
        }
    return SUCCESS;
};

void * Server::serverReceive(void* pvsocket) 
{ 
    socketArg_t *arg = (socketArg_t*) pvsocket;
    uint32_t dataLength;
    std::string receivedString;
    std::vector<char> rcvBuf;  
    while (true) 
    {  
    
        recv(arg->socket,&dataLength,sizeof(uint32_t),0); // Receive the message length
        dataLength = ntohl(dataLength ); // Ensure host system byte order
        rcvBuf.resize(dataLength,0x00);

        recv(arg->socket,&(rcvBuf[0]),dataLength,0); // Receive the string data
        receivedString.assign(&(rcvBuf[0]),rcvBuf.size()); 

        std::cout <<"Client: "<< receivedString << std::endl;

        rcvBuf.clear();
        receivedString.clear();

    }
    return SUCCESS;
};

int Server::serverSend(std::string dataToSend, int targetSocket) 
{

    uint32_t dataLength = htonl(dataToSend.size()); // Ensure network byte order when sending the data length

    if( (send(targetSocket,&dataLength ,sizeof(uint32_t), 0)) == -1 || 
        (send(targetSocket,dataToSend.c_str(),dataToSend.size(), 0)) == -1 )
        {
            std::cout << "send failed with error "  << std::endl;
            return(ERROR_SEND);
        }
    else
        serverReceiveRequest(targetSocket);
    return SUCCESS;
};

void Server::serverReceiveRequest(int targetSocket)
{
    int32_t dataLength;
    std::string receivedString;
    std::vector<char> rcvBuf;  
    recv(targetSocket,&dataLength,sizeof(uint32_t),0); // Receive the message length
    dataLength = ntohl(dataLength ); // Ensure host system byte order
    rcvBuf.resize(dataLength,0x00);
    recv(targetSocket,&(rcvBuf[0]),dataLength,0); // Receive the string data
    receivedString.assign(&(rcvBuf[0]),rcvBuf.size()); 
    std::cout <<"Client: "<< receivedString << std::endl;
    rcvBuf.clear();
    receivedString.clear();
}


    void Server::setServerSocket(int socket)
    {
        serverSocket=socket;
    };
    int Server::getServerSocket()
    {
        return serverSocket;
    };
    void Server::setClientSocket(int socket)
    {
        clientSocket=socket;
    };
    int Server::getClientSocket()
    {
        return clientSocket;
    };
    void Server::setServerPort(unsigned short port)
    {
        serverPort=port;
    };
    unsigned short Server::getServerPort()
    {
        return serverPort;
    };

    void Server::setServerAddr(sockaddr_in addr)
    {
        serverAddr.sin_addr.s_addr = addr.sin_addr.s_addr;
        serverAddr.sin_family = addr.sin_family;
        serverAddr.sin_port = addr.sin_port;
    };

    sockaddr_in Server::getServerAddr()
    {
        return serverAddr;
    };

    sockaddr_in Server::getClientAddr()
    {
        return clientAddr;
    };

    socketArg_t Server::getsocketArg()
    {
        return arg;
    };