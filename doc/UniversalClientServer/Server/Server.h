#pragma once
#include "../include/include.h"

typedef struct socketArg_tag {
    int socket;
} socketArg_t;

class Server
{
    private:   
    unsigned short serverPort;
    int serverSocket, clientSocket;
    sockaddr_in serverAddr, clientAddr;
    socketArg_t arg;
    socklen_t clientAddrSize;
    void setServerSocket(int);
    void setClientSocket(int);
    void setClientAddrSize(socklen_t);
    socklen_t getClientAddrSize();
    void setServerPort(unsigned short);
    unsigned short getServerPort();
    void setServerAddr(sockaddr_in);
    sockaddr_in getServerAddr();
    void setClientAddr(sockaddr_in);
    sockaddr_in getClientAddr();
    void serverReceiveRequest(int targetSocket);
    public:
    Server(uint16_t port);
    ~Server();
    int startServer();
    static void *serverReceive(void* pvsocket);
    int serverSend(std::string dataToSend, int targetSocket);
    socketArg_t getsocketArg();
    int closeServer();
    int acceptClient();
    int getServerSocket();
    int getClientSocket();
    
};