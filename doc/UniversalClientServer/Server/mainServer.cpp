#include "Server.h"

void showMenu()
{
    std::cout<<"-------------MENU---------------"<<std::endl;
    std::cout<<"1 - Send a message to the client"<<std::endl;
    std::cout<<"2 - Start server"<<std::endl;
    std::cout<<"3 - Add Client"<<std::endl;
    std::cout<<"4 - Close server"<<std::endl;
    std::cout<<"10 - Exit"<<std::endl;
    std::cout<<"0 - Show menu"<<std::endl;
};

int main() 
{
    Server server(5555);
    

    pthread_t treadIdReceive; 
    int status_addrReceive;
    int treadReceive;
    std::string dataToSend;
    socketArg_t argClientSocket;

    //uint16_t clientNum = 0;
    //uint16_t clientIdTarget;

    uint16_t pressButton;
    showMenu();
    while (true)
    {
        std::cout<<"Еnter the desired number in the menu (To display the menu, enter 0): "<<std::endl;
        std::cin>>pressButton;
        std::cin.ignore(32767, '\n');
        switch (pressButton)
        {
            case 0: 
            showMenu();
            break;
            case 1:
            //std::cout<<"Enter number client: "<<std::endl;
            //std::cin>>clientIdTarget;
            //std::cin.ignore(32767, '\n');
            if(server.getServerSocket() == -1)
                std::cout<<"The server is not running."<<std::endl;
            else if (server.getServerSocket() == -1)
                std::cout<<"The client is not connected."<<std::endl;
            else
            {
                std::cout<<"Enter the message you want to send: "<<std::endl;
                std::getline(std::cin,dataToSend);
                if(server.serverSend(dataToSend, argClientSocket.socket) !=SUCCESS)
                    std::cout<<"Failed to send message"<<std::endl;
                else 
                    sleep (1);
            }
            break;
            case 2:
            if(server.getServerSocket() == -1)
            {
                if(server.startServer() != SUCCESS)
                    std::cout<<"Failed to start the server."<<std::endl;
            }
            else
                std::cout<<"The server is already running."<<std::endl;    
            break;
            case 3:
            if(server.getClientSocket() == -1)
            {
            if( server.acceptClient() == SUCCESS)
            {
            argClientSocket=server.getsocketArg();
            //if ( (treadReceive = pthread_create(&treadIdReceive, NULL, &server.serverReceive, (void*)&argClientSocket)) != SUCCESS) 
              //  std::cout << "Thread Receive Creation Error. " << std::endl;
            //clientNum++;
            }
            else
                std::cout << "Failed to add client. "  << std::endl;
            }
            else
                std::cout<<"The client is already connected."<<std::endl;    
            break;
            case 4:
            //for(pthread_t n:treadIdReceive)
            pthread_cancel(treadIdReceive);
            server.~Server();
            break;
            case 10:
            //for(pthread_t n:treadIdReceive)
            pthread_cancel(treadIdReceive);
            server.closeServer();
            return 0;
            break;
            default:
            std::cout<<"You have selected a non-existent item. Please enter another number."<<std::endl;
            break;
        }
    }
    // treadReceive = pthread_join(treadIdReceive, (void**)&status_addrReceive);
    // if (treadReceive != SUCCESS) 
    //     { 
    //         std::cout << "Thread Joining Error: "  << std::endl;
    //         exit(ERROR_JOIN_THREAD);
    //     }
    server.closeServer();
    return 0;
}