#pragma once

#include <iostream> 
#include <cstdio> 
#include <cstring> 
#include <sys/socket.h>
#include <netinet/in.h>
#include <fcntl.h>
#include <unistd.h>
#include <pthread.h>

#include <arpa/inet.h>


#include <boost/variant.hpp>
#include <boost/optional.hpp>
#include <boost/algorithm/string.hpp>
#include <stdexcept>
#include <regex>
#include <nlohmann/json.hpp>

#include <fstream>
#include <stdio.h>
#include <sstream>


#define ERROR_CREATE_THREAD -11
#define ERROR_JOIN_THREAD   -12
#define ERROR_CREATE_SOCKET -13
#define ERROR_BIND_SOCKET   -14
#define ERROR_LISTEN_SOCKET -15
#define ERROR_RECV          -16
#define ERROR_SEND          -17
#define ERROR_CONNECTION    -18
#define ERROR_ACCEPT_CLIENT -19
#define ERROR_CLOSE_SERVER  -20
#define ERROR_CLOSE_CLIENT  -21

#define SUCCESS              0



