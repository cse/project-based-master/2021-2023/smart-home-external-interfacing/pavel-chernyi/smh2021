// Handlers.cpp 
 
#include "./Handlers.h"
 
#include <Poco/Net/HTTPRequestHandler.h>
#include <Poco/Net/HTTPServerResponse.h>
#include <Poco/Net/HTTPServerRequest.h>
#include <Poco/StreamCopier.h>
 
#include <iostream>

#include <nlohmann/json.hpp>
#include <fstream>
#include <iomanip>
#include <unistd.h>
#include "Listener.h"

void Devices::handleRequest(Poco::Net::HTTPServerRequest& request, Poco::Net::HTTPServerResponse& response)
{
	try
	{
		sleep(1);
		std::cout << "Get Devices Request:" << std::endl;

		if (request.has("Authorization") && Listener::sender.GetKeyAuth() == request.get("Authorization"))
		{
			std::cout << "Success Authorization" << std::endl;

			std::string request_id = request.get("X-Request-Id");
			std::cout << "Request_id: " << request_id << std::endl;

			nlohmann::json jsonDevicesInfo = JsonFromFile("DevicesInfo.json");

			if (jsonDevicesInfo != NULL)
			{
				jsonDevicesInfo["request_id"] = request_id;
				std::string response_string = jsonDevicesInfo.dump();

				response.setChunkedTransferEncoding(true);
				response.setContentType("application/json");
				std::ostream &ostr = response.send();
				ostr << response_string;
				response.setStatus(Poco::Net::HTTPServerResponse::HTTP_OK);
			}
		} 
		else
		{
			if (!request.has("Authorization"))
				std::cout << "Request has not header Authorization." << std::endl;
			else std::cout << "Not Authorization." << " WaitAuth:" << Listener::sender.GetKeyAuth() << ". RequestAuth:" << request.get("Authorization") << "." << std::endl;

			response.setStatus(Poco::Net::HTTPServerResponse::HTTP_UNAUTHORIZED);
			response.setReason("Unauthorized");
			response.send();
		}
	}
	catch (Poco::Exception &ex)
	{
		response.setStatus(Poco::Net::HTTPServerResponse::HTTP_BAD_REQUEST);
		response.setReason("Bad Request");
		response.send();

		std::cerr << "HTTP exception: " << ex.displayText() << ". Code:" << ex.code() << std::endl;
	}
	catch (nlohmann::json::exception &ex)
	{
		response.setStatus(Poco::Net::HTTPServerResponse::HTTP_BAD_REQUEST);
		response.setReason("Bad Request");
		response.send();

		std::cerr << "JSON exception: " << ex.what() << ". Id:" << ex.id << std::endl;
	}
	catch (std::ios_base::failure &ex)
	{
		response.setStatus(Poco::Net::HTTPServerResponse::HTTP_BAD_REQUEST);
		response.setReason("Bad Request");
		response.send();

		std::cerr << "File exception: " << ex.what() << ". Code:" << ex.code() << std::endl;
	}
}


void Condition::handleRequest(Poco::Net::HTTPServerRequest& request, Poco::Net::HTTPServerResponse& response)
{
	try
	{
		std::cout << "Post Condition Request:" << std::endl;

		if (request.has("Authorization") && Listener::sender.GetKeyAuth() == request.get("Authorization"))
		{
			std::cout << "Request_id: " << request.get("X-Request-Id") << std::endl;

			std::string recv_string;
			Poco::StreamCopier::copyToString(request.stream(), recv_string);
			std::cout << recv_string << std::endl;

			nlohmann::json jsonConditionInfo = JsonFromFile("ConditionDevices.json");

			if (jsonConditionInfo != NULL)
			{
				nlohmann::json jsonResponse;
				nlohmann::json jsonDeviceError;
				nlohmann::json jsonRecv = nlohmann::json::parse(recv_string);

				for (auto deviceRecv: jsonRecv.at("devices"))
				{
					bool deviceFind(false);

					for (auto deviceCondition: jsonConditionInfo.at("devices"))
					{
						if (deviceRecv.at("id") == deviceCondition.at("id"))
						{
							jsonResponse["devices"].push_back(deviceCondition);
							deviceFind = true;
							break;
						}
					}

					if (!deviceFind)
					{
						jsonDeviceError["id"] = deviceRecv.at("id");
						jsonDeviceError["error_code"] = "DEVICE_UNREACHABLE";
						jsonDeviceError["error_message"] = "Устройство не отвечает. Проверьте, вдруг оно выключено.";
						jsonResponse["devices"].push_back(jsonDeviceError);
					}
				}

				std::string request_id = request.get("X-Request-Id");
				jsonResponse["request_id"] = request_id;
				std::string response_string = jsonResponse.dump();

				response.setChunkedTransferEncoding(true);
				response.setContentType("application/json");
				std::ostream &ostr = response.send();
				ostr << response_string;
				response.setStatus(Poco::Net::HTTPServerResponse::HTTP_OK);
			}
		}
		else
		{
			if (!request.has("Authorization"))
				std::cout << "Request has not header Authorization." << std::endl;
			else std::cout << "Not Authorization." << " WaitAuth:" << Listener::sender.GetKeyAuth() << ". RequestAuth:" << request.get("Authorization") << "." << std::endl;

			response.setStatus(Poco::Net::HTTPServerResponse::HTTP_UNAUTHORIZED);
			response.setReason("Unauthorized");
			response.send();
		}
	}
	catch (Poco::Exception &ex)
	{
		response.setStatus(Poco::Net::HTTPServerResponse::HTTP_BAD_REQUEST);
		response.setReason("Bad Request");
		response.send();

		std::cerr << "HTTP exception: " << ex.displayText() << ". Code:" << ex.code() << std::endl;
	}
	catch (nlohmann::json::exception &ex)
	{
		response.setStatus(Poco::Net::HTTPServerResponse::HTTP_BAD_REQUEST);
		response.setReason("Bad Request");
		response.send();

		std::cerr << "JSON exception: " << ex.what() << ". Id:" << ex.id << std::endl;
	}
	catch (std::ios_base::failure &ex)
	{
		response.setStatus(Poco::Net::HTTPServerResponse::HTTP_BAD_REQUEST);
		response.setReason("Bad Request");
		response.send();

		std::cerr << "File exception: " << ex.what() << ". Code:" << ex.code() << std::endl;
	}
}


void Delete::handleRequest(Poco::Net::HTTPServerRequest& request, Poco::Net::HTTPServerResponse& response)
{
	try
	{
		std::cout << "Delete Devices Request:" << std::endl;

		if (request.has("Authorization") && Listener::sender.GetKeyAuth() == request.get("Authorization"))
		{
			//"The user deleted the external hub by using the user client"
			std::cout << "Request_id: " << request.get("X-Request-Id") << std::endl;

			std::string recv_string;
			Poco::StreamCopier::copyToString(request.stream(), recv_string);
			std::cout << recv_string << std::endl;

			nlohmann::json jsonDeviceInfo = JsonFromFile("DevicesInfo.json");

			if (jsonDeviceInfo != NULL)
			{
				nlohmann::json jsonRecv = nlohmann::json::parse(recv_string);

				for (auto &deviceDelete: jsonRecv.at("devices"))
				{
					bool devFound = false;

					for (auto &device: jsonDeviceInfo.at("devices"))
					{
						if (device.at("id") == deviceDelete.at("id"))
						{
							devFound = true;
							break;
						}
					}

					if (devFound)
						std::cout << "The user deleted device with ID: " << deviceDelete.at("id") << " in user client" << std::endl;
					else std::cout << "The user deleted device with ID: " << deviceDelete.at("id") << " in user client, but device with this ID was not found" << std::endl;
				}
				response.send();
				response.setStatus(Poco::Net::HTTPServerResponse::HTTP_OK);
			}
		}
		else
		{
			if (!request.has("Authorization"))
				std::cout << "Request has not header Authorization." << std::endl;
			else std::cout << "Not Authorization." << " WaitAuth:" << Listener::sender.GetKeyAuth() << ". RequestAuth:" << request.get("Authorization") << "." << std::endl;

			response.setStatus(Poco::Net::HTTPServerResponse::HTTP_UNAUTHORIZED);
			response.setReason("Unauthorized");
			response.send();
		}
	}
	catch (Poco::Exception &ex)
	{
		response.setStatus(Poco::Net::HTTPServerResponse::HTTP_BAD_REQUEST);
		response.setReason("Bad Request");
		response.send();

		std::cerr << "HTTP exception: " << ex.displayText() << ". Code:" << ex.code() << std::endl;
	}
	catch (nlohmann::json::exception &ex)
	{
		response.setStatus(Poco::Net::HTTPServerResponse::HTTP_BAD_REQUEST);
		response.setReason("Bad Request");
		response.send();

		std::cerr << "JSON exception: " << ex.what() << ". Id:" << ex.id << std::endl;
	}
	catch (std::ios_base::failure &ex)
	{
		response.setStatus(Poco::Net::HTTPServerResponse::HTTP_BAD_REQUEST);
		response.setReason("Bad Request");
		response.send();

		std::cerr << "File exception: " << ex.what() << ". Code:" << ex.code() << std::endl;
	}
}


void Action::handleRequest(Poco::Net::HTTPServerRequest& request, Poco::Net::HTTPServerResponse& response)
{
	try
	{
		std::cout << "Post Action Request:" << std::endl;

		if (request.has("Authorization") && Listener::sender.GetKeyAuth() == request.get("Authorization"))
		{
			std::cout << "Request_id: " << request.get("X-Request-Id") << std::endl;

			std::string recv_string;
			Poco::StreamCopier::copyToString(request.stream(), recv_string);
			std::cout << recv_string << std::endl;

			nlohmann::json jsonConditionInfo = JsonFromFile("ConditionDevices.json");

			if (jsonConditionInfo != NULL)
			{
				nlohmann::json jsonNewCondition = nlohmann::json::parse(recv_string);

				nlohmann::json jsonConditionStatus;
				jsonConditionStatus["request_id"] = request.get("X-Request-Id");

				nlohmann::json jsonTempDevice, jsonTempCapability, jsonTempState, jsonTempActionResult;
				bool flDeviceFound, flCapabilityInstanceFound;

				for (const auto &itemNewCondition: jsonNewCondition.at("devices"))
				{
					flDeviceFound = false;
					jsonTempDevice["id"] = itemNewCondition.at("id");

					for (auto &itemOldCondition: jsonConditionInfo.at("devices"))
					{

						if (itemNewCondition.at("id") == itemOldCondition.at("id"))
						{
							flDeviceFound = true;

							for (const auto &itemCapabilityNewCondition: itemNewCondition.at("capabilities"))
							{
								flCapabilityInstanceFound = false;
								jsonTempCapability["type"] = itemCapabilityNewCondition.at("type");
								jsonTempState["instance"] = itemCapabilityNewCondition.at("state").at("instance");

								for (auto &itemCapabilityOldCondition: itemOldCondition.at("capabilities"))
								{

									if (itemCapabilityNewCondition.at("type") == itemCapabilityOldCondition.at("type"))
									{

										if (itemCapabilityNewCondition.at("state").at("instance") ==
										    itemCapabilityOldCondition.at("state").at("instance"))
										{
											flCapabilityInstanceFound = true;
											jsonTempActionResult["status"] = "DONE";
											jsonTempState["action_result"] = jsonTempActionResult;
											itemCapabilityOldCondition["state"]["value"] = itemCapabilityNewCondition.at("state").at("value");
										}
									}
								}

								if (!flCapabilityInstanceFound)
								{
									jsonTempActionResult["status"] = "ERROR";
									jsonTempActionResult["error_code"] = "INVALID_ACTION";
									jsonTempActionResult["error_message"] = "the human readable error message";

									jsonTempState["action_result"] = jsonTempActionResult;
								}

								jsonTempCapability["state"] = jsonTempState;
								jsonTempDevice["capabilities"].push_back(jsonTempCapability);

								jsonTempState.clear();
								jsonTempCapability.clear();
								jsonTempActionResult.clear();
							}
						}
					}

					if (!flDeviceFound)
					{
						jsonTempActionResult["status"] = "ERROR";
						jsonTempActionResult["error_code"] = "DEVICE_UNREACHABLE";
						jsonTempActionResult["error_message"] = "the human readable error message";
						jsonTempDevice["action_result"] = jsonTempActionResult;

						jsonTempActionResult.clear();
					}

					jsonConditionStatus["devices"].push_back(jsonTempDevice);
					jsonTempDevice.clear();
				}

				std::ofstream fileNewConditionInfo("ConditionDevices.json");
				fileNewConditionInfo.exceptions(std::ifstream::failbit | std::ifstream::badbit);
				fileNewConditionInfo << std::setw(4) << jsonConditionInfo << std::endl;

				std::string response_string = jsonConditionStatus.dump();
				std::cout << response_string << std::endl;

				response.setChunkedTransferEncoding(true);
				response.setContentType("application/json");
				std::ostream &ostr = response.send();
				ostr << response_string;
				response.setStatus(Poco::Net::HTTPServerResponse::HTTP_OK);
			}
		}
		else
		{
		
			if (!request.has("Authorization"))
				std::cout << "Request has not header Authorization." << std::endl;
			else std::cout << "Not Authorization." << " WaitAuth:" << Listener::sender.GetKeyAuth() << ". RequestAuth:" << request.get("Authorization") << "." << std::endl;

			response.setStatus(Poco::Net::HTTPServerResponse::HTTP_UNAUTHORIZED);
			response.setReason("Unauthorized");
			response.send();
		}
	}
	catch (Poco::Exception &ex)
	{
		response.setStatus(Poco::Net::HTTPServerResponse::HTTP_BAD_REQUEST);
		response.setReason("Bad Request");
		response.send();

		std::cerr << "HTTP exception: " << ex.displayText() << ". Code:" << ex.code() << std::endl;
	}
	catch (nlohmann::json::exception &ex)
	{
		response.setStatus(Poco::Net::HTTPServerResponse::HTTP_BAD_REQUEST);
		response.setReason("Bad Request");
		response.send();

		std::cerr << "JSON exception: " << ex.what() << ". Id:" << ex.id << std::endl;
	}
	catch (std::ios_base::failure &ex)
	{
		response.setStatus(Poco::Net::HTTPServerResponse::HTTP_BAD_REQUEST);
		response.setReason("Bad Request");
		response.send();

		std::cerr << "File exception: " << ex.what() << ". Code:" << ex.code() << std::endl;
	}
}


void Leave::handleRequest(Poco::Net::HTTPServerRequest& request, Poco::Net::HTTPServerResponse& response)
{
	try
	{
		std::cout << "Post Leave Request:" << std::endl;

		if (request.has("Authorization") && Listener::sender.GetKeyAuth() == request.get("Authorization"))
		{
			std::cout << "Request_id: " << request.get("X-Request-Id") << std::endl;

			std::string recv_string;
			Poco::StreamCopier::copyToString(request.stream(), recv_string);
			std::cout << recv_string << std::endl;

			Listener::sender.handlerChosenNumRequest(5);//Send Leave

			int codeReturn = Listener::sender.NewKeyAuth();
			if (codeReturn == 0)
				std::cout << "The authorization key has been changed to " << Listener::sender.GetKeyAuth() << std::endl;
			else
				std::cout << "The authorization key has not been changed. Exception code: " << codeReturn << std::endl;

			response.send();
			response.setStatus(Poco::Net::HTTPServerResponse::HTTP_OK);
		}
		else
		{
			if (!request.has("Authorization"))
				std::cout << "Request has not header Authorization." << std::endl;
			else std::cout << "Not Authorization." << " WaitAuth:" << Listener::sender.GetKeyAuth() << ". RequestAuth:" << request.get("Authorization") << "." << std::endl;

			response.setStatus(Poco::Net::HTTPServerResponse::HTTP_UNAUTHORIZED);
			response.setReason("Unauthorized");
			response.send();
		}
	}
	catch (Poco::Exception &ex)
	{
		response.setStatus(Poco::Net::HTTPServerResponse::HTTP_BAD_REQUEST);
		response.setReason("Bad Request");
		response.send();

		std::cerr << "HTTP exception: " << ex.displayText() << ". Code:" << ex.code() << std::endl;
	}
	catch (nlohmann::json::exception &ex)
	{
		response.setStatus(Poco::Net::HTTPServerResponse::HTTP_BAD_REQUEST);
		response.setReason("Bad Request");
		response.send();

		std::cerr << "JSON exception: " << ex.what() << ". Id:" << ex.id << std::endl;
	}
	catch (std::ios_base::failure &ex)
	{
		response.setStatus(Poco::Net::HTTPServerResponse::HTTP_BAD_REQUEST);
		response.setReason("Bad Request");
		response.send();

		std::cerr << "File exception: " << ex.what() << ". Code:" << ex.code() << std::endl;
	}
}

void PutInfo::handleRequest(Poco::Net::HTTPServerRequest& request, Poco::Net::HTTPServerResponse& response)
{
	try
	{
		std::string recv_string;
		Poco::StreamCopier::copyToString(request.stream(), recv_string);

		std::cout << recv_string << std::endl;

		nlohmann::json jsonRecv = nlohmann::json::parse(recv_string);

		Listener::sender.SetUri(jsonRecv.at("ip"), jsonRecv.at("port"));
		response.setStatus(Poco::Net::HTTPServerResponse::HTTP_OK);
		response.send();
	}
	catch (Poco::Exception &ex)
	{
		response.setStatus(Poco::Net::HTTPServerResponse::HTTP_BAD_REQUEST);
		response.setReason("Bad Request");
		response.send();

		std::cerr << "HTTP exception: " << ex.displayText() << ". Code:" << ex.code() << std::endl;
	}
	catch (nlohmann::json::exception &ex)
	{
		response.setStatus(Poco::Net::HTTPServerResponse::HTTP_BAD_REQUEST);
		response.setReason("Bad Request");
		response.send();

		std::cerr << "JSON exception: " << ex.what() << ". Id:" << ex.id << std::endl;
	}
}

void GetInfo::handleRequest(Poco::Net::HTTPServerRequest& request, Poco::Net::HTTPServerResponse& response)
{
	try
	{
		nlohmann::json jsonTempRequest;
		jsonTempRequest["hub_id"] = Listener::sender.GetHubId();
		jsonTempRequest["uri"] = Listener::sender.GetUri();
		jsonTempRequest["key"] = Listener::sender.GetKeyAuth();

		std::string response_string = jsonTempRequest.dump();

		response.setChunkedTransferEncoding(true);
		response.setContentType("application/json");
		std::ostream &ostr = response.send();
		ostr << jsonTempRequest;
		response.setStatus(Poco::Net::HTTPServerResponse::HTTP_OK);
	}
	catch (Poco::Exception &ex)
	{
		response.setStatus(Poco::Net::HTTPServerResponse::HTTP_BAD_REQUEST);
		response.setReason("Bad Request");
		response.send();

		std::cerr << "HTTP exception: " << ex.displayText() << ". Code:" << ex.code() << std::endl;
	}
	catch (nlohmann::json::exception &ex)
	{
		response.setStatus(Poco::Net::HTTPServerResponse::HTTP_BAD_REQUEST);
		response.setReason("Bad Request");
		response.send();

		std::cerr << "JSON exception: " << ex.what() << ". Id:" << ex.id << std::endl;
	}
}

void DoSomething::handleRequest(Poco::Net::HTTPServerRequest &request, Poco::Net::HTTPServerResponse &response)
{
	try
	{
		Poco::URI uri(request.getURI());
		int codeReq = Listener::sender.handlerChosenNumRequest(std::stoi(uri.getQuery()));
		nlohmann::json jsonTempRequest;

		if(codeReq == 0)
			jsonTempRequest["status"] = "Done";
		else
		{
			jsonTempRequest["status"] = "Error";
			jsonTempRequest["error_code"] = codeReq;

			if(codeReq == httpEx)
				jsonTempRequest["error_message"] = "POCO HTTP error";
			else if(codeReq == jsonEx)
				jsonTempRequest["error_message"] = "Nlohmann JSON error";
			else if(codeReq == iosEx)
				jsonTempRequest["error_message"] = "IOS error";
			else jsonTempRequest["error_message"] = "Unknown error";
		}
		response.setChunkedTransferEncoding(true);
		response.setContentType("application/json");
		std::ostream &ostr = response.send();
		ostr << jsonTempRequest;
		response.setStatus(Poco::Net::HTTPServerResponse::HTTP_OK);
	}
	catch (Poco::Exception &ex)
	{
		response.setStatus(Poco::Net::HTTPServerResponse::HTTP_BAD_REQUEST);
		response.setReason("Bad Request");
		response.send();

		std::cerr << "HTTP exception: " << ex.displayText() << ". Code:" << ex.code() << std::endl;
	}
}
////////DELETE DEVICES//////////////////
/*nlohmann::json jsonConditionInfo = JsonFromFile("ConditionDevices.json");
			nlohmann::json jsonDeviceInfo = JsonFromFile("DevicesInfo.json");
			if (jsonConditionInfo != NULL && jsonDeviceInfo != NULL)
			{
				nlohmann::json jsonRecv = nlohmann::json::parse(recv_string);
				for (auto &deviceDelete: jsonRecv.at("devices"))
					for (long unsigned int i = 0; i < jsonDeviceInfo.at("devices").size(); i++)
					{
						if (jsonDeviceInfo.at("devices")[i].at("id") == deviceDelete.at("id"))
						{
							jsonDeviceInfo.at("devices").erase(i);
							break;
						}
					}
				for (auto &deviceDelete: jsonRecv.at("devices"))
					for (long unsigned int i = 0; i < jsonConditionInfo.at("devices").size(); i++)
					{
						if (jsonConditionInfo.at("devices")[i].at("id") == deviceDelete.at("id"))
						{
							jsonConditionInfo.at("devices").erase(i);
							break;
						}
					}
				JsonToFile(jsonDeviceInfo, "DevicesInfo.json");
				JsonToFile(jsonConditionInfo, "ConditionDevices.json");*/