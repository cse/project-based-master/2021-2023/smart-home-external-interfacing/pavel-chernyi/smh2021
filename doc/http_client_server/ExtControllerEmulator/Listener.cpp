#include "Listener.h"
#include <signal.h>
#include <exception>
#include <Poco/Net/HTTPServer.h>
#include <string>
#include <iostream>

Sender Listener::sender;

Listener::Listener(uint16_t port)
{
	StartServer(port);
}

uint16_t Listener::StartServer(uint16_t port)
{
	try
	{
		Poco::Net::HTTPServerParams *params = new Poco::Net::HTTPServerParams();
		params->setMaxQueued(50);
		params->setMaxThreads(4);

		Poco::Net::ServerSocket socket(port);

		Poco::Net::HTTPServer server(new CommonRequestHandler(), socket, params);

		server.start();
		sigset_t set;
		sigemptyset(&set);
		sigaddset(&set, SIGINT);
		int sig;
		sigwait(&set, &sig);
		server.stop();
	}
	catch (Poco::Exception &ex)
	{
		std::cerr << "HTTP exception: " << ex.what() << ". Code:" << ex.code() << std::endl;
		return httpEx;
	}
	catch (std::exception &ex)
	{
		std::cerr << "Other exception: " << ex.what() << std::endl;
		return -2;
	}
	return 0;
}

Listener::~Listener()=default;
