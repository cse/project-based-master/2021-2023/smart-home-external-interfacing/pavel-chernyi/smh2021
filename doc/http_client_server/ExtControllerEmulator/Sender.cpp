//Sender.cpp
#include "Sender.h"
#include <unistd.h>
#include <ext/stdio_filebuf.h>
#include <sys/file.h>

Sender::Sender()
{
	try
	{
		_uri = ("http://localhost:60000");
		nlohmann::json jsonClientInfo = JsonFromFile("ClientInfo.json");
		if (jsonClientInfo != NULL)
		{
			_keyAuth = jsonClientInfo.at("key_authorization");
			_hubId = jsonClientInfo.at("hub_id");
		}
	}
	catch (Poco::Exception &ex)
	{
		std::cerr << "HTTP exception: " << ex.displayText() << ". Code:" << ex.code() << std::endl;
		//return httpEx;
	}
	catch (nlohmann::json::exception &ex)
	{
		std::cerr << "JSON exception: " << ex.what() << ". Id:" << ex.id << std::endl;
		//return jsonEx;
	}
}

int16_t Sender::handlerChosenNumRequest(const uint16_t& numberRequest) {
    switch (numberRequest) {
	    case 0:
		    return smarthomeTestAlarm();
        case 1:
	        return smarthomeIntroduction();
	    case 2:
		    return smarthomeActionNotification();
	    case 3:
		    return smarthomeParametersNotification();
	    case 4:
		    return smarthomeErrorsNotification();
	    case 5:
		    return smarthomeLeave();
        default:
        {
			std::cout<<"no request under the selected number"<<std::endl;
	        return -1;
		}
    }
}


int16_t Sender::smarthomeIntroduction()
{
	try
	{
		nlohmann::json jsonClientInfo = JsonFromFile("ClientInfo.json");
		if (jsonClientInfo != NULL)
		{
			Poco::Net::HTTPRequest request(Poco::Net::HTTPRequest::HTTP_POST, "/v1.3/smarthome/ext_controller/introduction", Poco::Net::HTTPMessage::HTTP_1_1);

			Poco::Net::HTTPResponse res;
			std::string recvString;
			int codeReturnSend = Send(request, jsonClientInfo, res, &recvString, false);
			if(codeReturnSend == 0)
			{
				std::cout << "Introduction response:" << std::endl;
				std::cout << res.getVersion() << " " << res.getStatus() << " " << res.getReason() << std::endl;
				std::cout << recvString << std::endl;
				nlohmann::json recvJson = nlohmann::json::parse(recvString);
				if(recvJson.at("hub_id") == -1)
				{
					std::cerr << "Something was wrong with ClientInfo.json. Hub_id rewrite to 0. Do Introduction again!" << std::endl;
					jsonClientInfo["hub_id"] = 0;
					JsonToFile(jsonClientInfo, "ClientInfo.json");
					return jsonEx;
				}
				_hubId = recvJson.at("hub_id");
				jsonClientInfo["hub_id"] = _hubId;

				codeReturnSend = JsonToFile(jsonClientInfo, "ClientInfo.json");
			}
			return codeReturnSend;
		}
		else return jsonEx;
	}
	catch (Poco::Exception &ex)
	{
		std::cerr << "HTTP exception: " << ex.displayText() << ". Code:" << ex.code() << std::endl;
		return httpEx;
	}
	catch (nlohmann::json::exception &ex)
	{
		std::cerr << "JSON exception: " << ex.what() << ". Id:" << ex.id << std::endl;
		return jsonEx;
	}
	catch (std::ios_base::failure &ex)
	{
		std::cerr << "File exception: " << ex.what() << ". Code:" << ex.code() << std::endl;
		return iosEx;
	}
}


int16_t Sender::smarthomeTestAlarm()
{
	try
	{
		bool alarm = true;
		for (int i = 0; i < 2; i++)
		{
			sleep(rand() % 20 + 10);
			std::cout << "sending alarm " << std::endl;
			SendAlarm(alarm);
			std::cout << "alarm sent !" << std::endl;
			alarm = !alarm;
		}
	}
	catch (Poco::Exception &ex)
	{
		std::cerr << "HTTP exception: " << ex.displayText() << ". Code:" << ex.code() << std::endl;
		return httpEx;
	}
	catch (nlohmann::json::exception &ex)
	{
		std::cerr << "JSON exception: " << ex.what() << ". Id:" << ex.id << std::endl;
		return jsonEx;
	}
	catch (std::ios_base::failure &ex)
	{
		std::cerr << "File exception: " << ex.what() << ". Code:" << ex.code() << std::endl;
		return iosEx;
	}
	return 0;
}

int16_t Sender::smarthomeActionNotification()
{
	try
	{
		nlohmann::json jsonActionNotification = JsonFromFile("ActionNotification.json");
		if (jsonActionNotification != NULL)
		{
			jsonActionNotification["hub_id"] = _hubId;

			Poco::Net::HTTPClientSession session(_uri.getHost(), _uri.getPort());
			session.setKeepAlive(true);

			Poco::Net::HTTPRequest request(Poco::Net::HTTPRequest::HTTP_POST, "/v1.3/smarthome/ext_controller/action_notification", Poco::Net::HTTPMessage::HTTP_1_1);

			Poco::Net::HTTPResponse res;
			std::string recvString;
			int codeReturnSend = Send(request, jsonActionNotification, res, std::nullopt);
			if(codeReturnSend == 0)
			{
				std::cout << "Action Notification response:" << std::endl;
				std::cout << res.getVersion() << " " << res.getStatus() << " " << res.getReason() << std::endl;
			}
			return codeReturnSend;
		} else return jsonEx;
	}
	catch (Poco::Exception &ex)
	{
		std::cerr << "HTTP exception: " << ex.displayText() << ". Code:" << ex.code() << std::endl;
		return httpEx;
	}
	catch (nlohmann::json::exception &ex)
	{
		std::cerr << "JSON exception: " << ex.what() << ". Id:" << ex.id << std::endl;
		return jsonEx;
	}
	catch (std::ios_base::failure &ex)
	{
		std::cerr << "File exception: " << ex.what() << ". Code:" << ex.code() << std::endl;
		return iosEx;
	}
}


int16_t Sender::smarthomeParametersNotification()
{
	try
	{
		nlohmann::json jsonParametersNotification = JsonFromFile("ParametersNotification.json");
		if (jsonParametersNotification != NULL)
		{
			jsonParametersNotification["hub_id"] = _hubId;
			std::string stringRequest = jsonParametersNotification.dump();

			Poco::Net::HTTPClientSession session(_uri.getHost(), _uri.getPort());
			session.setKeepAlive(true);

			Poco::Net::HTTPRequest request(Poco::Net::HTTPRequest::HTTP_POST, "/v1.3/smarthome/ext_controller/parameters_notification", Poco::Net::HTTPMessage::HTTP_1_1);
			request.setContentType("application/json");
			request.add("Authorization", _keyAuth);
			request.setContentLength(stringRequest.length());
			session.sendRequest(request) << stringRequest;

			// get response
			Poco::Net::HTTPResponse res;
			std::string recvString;
			std::istream &is = session.receiveResponse(res);
			std::cout << "Parameters Notification response:" << std::endl;
			std::cout << res.getVersion() << " " << res.getStatus() << " " << res.getReason() << std::endl;
			Poco::StreamCopier::copyToString(is, recvString);
			std::cout << recvString << std::endl;
		}
	}
	catch (Poco::Exception &ex)
	{
		std::cerr << "HTTP exception: " << ex.displayText() << ". Code:" << ex.code() << std::endl;
		return httpEx;
	}
	catch (nlohmann::json::exception &ex)
	{
		std::cerr << "JSON exception: " << ex.what() << ". Id:" << ex.id << std::endl;
		return jsonEx;
	}
	catch (std::ios_base::failure &ex)
	{
		std::cerr << "File exception: " << ex.what() << ". Code:" << ex.code() << std::endl;
		return iosEx;
	}
	return 0;
}


int16_t Sender::smarthomeErrorsNotification()
{
	try
	{
		nlohmann::json jsonErrorsNotification = JsonFromFile("ErrorsNotification.json");
		if (jsonErrorsNotification != NULL)
		{
			jsonErrorsNotification["hub_id"] = _hubId;
			std::string stringRequest = jsonErrorsNotification.dump();

			Poco::Net::HTTPClientSession session(_uri.getHost(), _uri.getPort());
			session.setKeepAlive(true);

			Poco::Net::HTTPRequest request(Poco::Net::HTTPRequest::HTTP_POST, "/v1.3/smarthome/ext_controller/errors_notification", Poco::Net::HTTPMessage::HTTP_1_1);
			request.setContentType("application/json");
			request.add("Authorization", _keyAuth);
			request.setContentLength(stringRequest.length());
			session.sendRequest(request) << stringRequest;

			// get response
			Poco::Net::HTTPResponse res;
			std::string recvString;
			std::istream &is = session.receiveResponse(res);
			std::cout << "Errors Notification response:" << std::endl;
			std::cout << res.getVersion() << " " << res.getStatus() << " " << res.getReason() << std::endl;
			Poco::StreamCopier::copyToString(is, recvString);
			std::cout << recvString << std::endl;
		}
	}
	catch (Poco::Exception &ex)
	{
		std::cerr << "HTTP exception: " << ex.displayText() << ". Code:" << ex.code() << std::endl;
		return httpEx;
	}
	catch (nlohmann::json::exception &ex)
	{
		std::cerr << "JSON exception: " << ex.what() << ". Id:" << ex.id << std::endl;
		return jsonEx;
	}
	catch (std::ios_base::failure &ex)
	{
		std::cerr << "File exception: " << ex.what() << ". Code:" << ex.code() << std::endl;
		return iosEx;
	}
	return 0;
}


int16_t Sender::smarthomeLeave()
{
	try
	{
		nlohmann::json jsonLeave;
		jsonLeave["hub_id"] = _hubId;
		jsonLeave["reason_id"] = 1;
		jsonLeave["reason"] = "The user disconnected the association";

		Poco::Net::HTTPRequest request(Poco::Net::HTTPRequest::HTTP_POST, "/v1.3/smarthome/ext_controller/leave", Poco::Net::HTTPMessage::HTTP_1_1);
		Poco::Net::HTTPResponse res;
		int codeReturnSend = Send(request, jsonLeave, res, std::nullopt);
		if (codeReturnSend == 0)
		{
			std::cout << "Leave response: " << std::endl;
			std::cout << res.getVersion() << " " << res.getStatus() << " " << res.getReason() << std::endl;
			int codeReturn = NewKeyAuth();
			if (codeReturn == 0)
			{
				std::cout << "The authorization key has been changed to " << _keyAuth << std::endl;
				nlohmann::json jsonClientInfo = JsonFromFile("ClientInfo.json");
				if (jsonClientInfo != NULL)
				{
					_hubId = 0;
					jsonClientInfo.at("hub_id") = _hubId;
					jsonClientInfo.at("key_authorization") = _keyAuth;
					JsonToFile(jsonClientInfo, "ClientInfo.json");
				}
			}
			else
				std::cout << "The authorization key has not been changed. Exception code: " << codeReturn << std::endl;
			return 0;
		}
		else
		{
			std::cout<<"Something went wrong!"<<std::endl;
			return codeReturnSend;
		}
	}
	catch (Poco::Exception &ex)
	{
		std::cerr << "HTTP exception: " << ex.displayText() << ". Code:" << ex.code() << std::endl;
		return httpEx;
	}
	catch (nlohmann::json::exception &ex)
	{
		std::cerr << "JSON exception: " << ex.what() << ". Id:" << ex.id << std::endl;
		return jsonEx;
	}
	catch (std::ios_base::failure &ex)
	{
		std::cerr << "File exception: " << ex.what() << ". Code:" << ex.code() << std::endl;
		return iosEx;
	}
	return 0;
}

Sender::~Sender() = default;

int16_t Sender::SendAlarm(const bool& alarm)
{
	nlohmann::json jsonAlarm = JsonFromFile("Alarm.json");
	if(jsonAlarm != NULL)
	{
		jsonAlarm["devices"][0]["properties"][0]["state"]["value"] = alarm ? "opened" : "closed";
		jsonAlarm["hub_id"] = _hubId;
		Poco::Net::HTTPRequest request(Poco::Net::HTTPRequest::HTTP_POST, "/v1.3/smarthome/ext_controller/action_notification", Poco::Net::HTTPMessage::HTTP_1_1);
		Poco::Net::HTTPResponse res;
		int codeReturnSend = Send(request, jsonAlarm, res, std::nullopt);
		if (codeReturnSend == 0)
		{
			std::cout << "Alarm response: " << std::endl;
			std::cout << res.getVersion() << " " << res.getStatus() << " " << res.getReason() << std::endl;
			return 0;
		}
		else
		{
			std::cout<<"Something went wrong!"<<std::endl;
			return codeReturnSend;
		}
	}
	else return jsonEx;
}

inline size_t AlignDown(size_t value, size_t boundary)
{
	if (boundary == 0)
		return 0;
	return boundary * (value / boundary);
}

std::string Base64Encode(const std::string& src)
{
	const std::string base64Chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

	std::string result;

	size_t off = 0;
	for (; off < AlignDown(src.size(), (size_t)3); off += 3)
	{
		unsigned char byte1 = src[off], byte2 = src[off + 1], byte3 = src[off + 2];
		result += base64Chars[(byte1 & 0xfc) >> 2];
		result += base64Chars[((byte1 & 0x03) << 4) + ((byte2 & 0xf0) >> 4)];
		result += base64Chars[((byte2 & 0x0f) << 2) + ((byte3 & 0xc0) >> 6)];
		result += base64Chars[byte3 & 0x3f];
	}

	if (off != src.size())
	{
		std::string tail(src.substr(off));
		unsigned char byte1 = tail[0], byte2 = tail.size() > 1 ? tail[1] : 0, byte3 = 0;
		result += base64Chars[(byte1 & 0xfc) >> 2];
		result += base64Chars[((byte1 & 0x03) << 4) + ((byte2 & 0xf0) >> 4)];
		result += tail.size() > 1 ? base64Chars[((byte2 & 0x0f) << 2) + ((byte3 & 0xc0) >> 6)] : '=';
		result += '=';
	}

	return result;
}

int16_t Sender::Send(Poco::Net::HTTPRequest& req, const nlohmann::json& js, Poco::Net::HTTPResponse& response, std::optional<std::string*> resStr, const bool& WithAuth)
{
	Poco::Net::HTTPClientSession session;
	try
	{
		session.setHost(_uri.getHost());
		session.setPort(_uri.getPort());
		session.setKeepAlive(true);
		req.setContentType("application/json");
		if (WithAuth)
			req.add("Authorization", "Token " + Base64Encode(_keyAuth));
		std::string reqStr = js.dump();
		req.setContentLength(reqStr.length());
		session.sendRequest(req) << js;
		std::istream &is = session.receiveResponse(response);
		if(resStr)
		{
			Poco::StreamCopier::copyToString(is, **resStr);
		}
		return 0;
	}
	catch (Poco::Exception &ex)
	{
		std::cerr << "HTTP exception: " << ex.displayText() << ". Code:" << ex.code() << std::endl;
		session.reset();
		return httpEx;
	}
	catch (nlohmann::json::exception &ex)
	{
		std::cerr << "JSON exception: " << ex.what() << ". Id:" << ex.id << std::endl;
		return jsonEx;
	}
	catch (std::ios_base::failure &ex)
	{
		std::cerr << "File exception: " << ex.what() << ". Code:" << ex.code() << std::endl;
		return iosEx;
	}
}

void Sender::SetUri(std::string ip, std::string port)
{
	_uri = ("http://"+ip+":"+port);
}


std::string Sender::GetUri()
{
	return _uri.toString();
}

std::string Sender::GetKeyAuth()
{
	return _keyAuth;
}

uint16_t Sender::GetHubId()
{
	return _hubId;
}


#include <errno.h>//TODO: Delete after all testing
nlohmann::json JsonFromFile(const std::string& fileName)
{
	try
	{
		std::ifstream myFile(fileName);
		if(myFile.is_open())
		{
			int fd = static_cast< __gnu_cxx::stdio_filebuf<char> *const >( myFile.rdbuf())->fd();
			if (flock(fd, LOCK_SH) != 0)
				std::cout << "Flock SH:" << errno << std::endl;//TODO: Delete after all testing
			myFile.exceptions(std::ifstream::failbit | std::ifstream::badbit);
			nlohmann::json myJson;
			myFile >> myJson;
			myFile.close();
			return myJson;
		}
		else return NULL;
	}
	catch (std::ios_base::failure &ex)
	{
		std::cerr << "File" << fileName << " exception: " << ex.what() << ". Code:" << ex.code() << std::endl;
		return NULL;
	}
	catch (nlohmann::json::exception &ex)
	{
		std::cerr << "JSON exception: " << ex.what() << ". Id:" << ex.id << std::endl;
		return NULL;
	}
}


int16_t JsonToFile(nlohmann::json myJson, const std::string &fileName)
{
	try
	{
		std::ofstream myFile(fileName);
		int fd = static_cast< __gnu_cxx::stdio_filebuf< char > * const >( myFile.rdbuf() )->fd();
		if(flock(fd, LOCK_EX)!=0)
			std::cout<<"Flock EX:" << errno << std::endl;//TODO: Delete after all testing
		myFile.exceptions(std::ifstream::failbit | std::ifstream::badbit);
		myFile << std::setw(4) << myJson << std::endl;
		return 0;
	}
	catch (nlohmann::json::exception &ex)
	{
		std::cerr << "JSON exception: " << ex.what() << ". Id:" << ex.id << std::endl;
		return jsonEx;
	}
	catch (std::ios_base::failure &ex)
	{
		std::cerr << "File" << fileName << " exception: " << ex.what() << ". Code:" << ex.code() << std::endl;
		return iosEx;
	}
}


std::string Sender::KeyGeneration()
{
	std::mt19937 gen{static_cast<unsigned long>(time(nullptr))};
	std::vector<std::uniform_int_distribution<> > ascii =
			{
					std::uniform_int_distribution<>('a', 'z'),
					std::uniform_int_distribution<>('A', 'Z'),
					std::uniform_int_distribution<>('0', '9'),
			};
	ascii.emplace_back(0, ascii.size() - 1);
	std::string password;
	for (std::size_t i = 0; i < 4; ++i)
	{
		for (std::size_t j = 0; j < 4; ++j)
		{
			password += static_cast<char>(ascii[ascii.back()(gen)](gen));
		}
		if(i < 3)
		password += '-';
	}
	return password;
}


int16_t Sender::NewKeyAuth()
{
	nlohmann::json jsonClientInfo = JsonFromFile("ClientInfo.json");
	if (jsonClientInfo != NULL)
	{
		if (jsonClientInfo.contains("key_authorization"))
		{
			_keyAuth=KeyGeneration();
			jsonClientInfo["key_authorization"] = _keyAuth;
			return JsonToFile(jsonClientInfo, "ClientInfo.json");
		}
	}
	return jsonEx;
}
