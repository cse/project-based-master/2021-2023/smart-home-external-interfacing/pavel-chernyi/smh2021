// main.cpp

#include "Listener.h"
#include <iostream>

int main(int argc, char* argv[]) {
	if (argc == 2)
	{
		Listener listener(std::stoi(argv[1]));
	} else {
		std::cout << "Num arguments not 2" << std::endl;
	}
	return 0;
}