// CommonRequestHandler.cpp 
 
#include <Poco/Net/HTTPServerRequest.h>
#include <Poco/URI.h>
 
#include "./Handlers.h"
#include "./CommonRequestHandler.h"
#include <iostream>
 
CommonRequestHandler::CommonRequestHandler() 
{
}
 
Poco::Net::HTTPRequestHandler* CommonRequestHandler::createRequestHandler (
    const Poco::Net::HTTPServerRequest& request)
{
	Poco::URI uri(request.getURI());

	if (request.getMethod() == "GET")
	{
		if (uri.getPath() == "/v1.0/ext_controller/devices")
			return new Devices();
		if (uri.getPath() == "/v1.0/ext_controller/info")
			return new GetInfo();
	} else if (request.getMethod() == "POST")
	{
		if (uri.getPath() == "/v1.0/ext_controller/condition")
		{
			return new Condition();
		} else if (uri.getPath() == "/v1.0/ext_controller/do")
		{
			return new DoSomething();
		} else if (uri.getPath() == "/v1.0/ext_controller/action")
		{
			return new Action();
		} else if (uri.getPath() == "/v1.0/ext_controller/leave")
		{
			return new Leave();
		}
	}
	else if (request.getMethod() == "PUT" && uri.getPath() == "/v1.0/ext_controller/info")
	{
		return new PutInfo();
	}
	return nullptr;
};