// Handlers.cpp 
 
#include "./Handlers.h"
 
#include <Poco/Net/HTTPRequestHandler.h>
#include <Poco/Net/HTTPServerResponse.h>
#include <Poco/Net/HTTPServerRequest.h>
#include <Poco/StreamCopier.h>
 
#include <iostream>

#include <nlohmann/json.hpp>
#include <fstream>
#include <iomanip>
#include <unistd.h>
#include "Listener.h"

void Devices::handleRequest(Poco::Net::HTTPServerRequest& request, Poco::Net::HTTPServerResponse& response)
{
	try
	{
		std::cout << "It`s GetDevicesReq" << std::endl;
		sleep(1);
		nlohmann::json jsonClientInfo = JsonFromFile("ClientInfo.json");
		if (jsonClientInfo != NULL)
		{
			if (jsonClientInfo.at("key_authorization") == request.get("Authorization"))
			{
				std::cout << "Success Authorization" << std::endl;
				std::string request_id = request.get("X-Request-Id");
				uint16_t hub_id = jsonClientInfo.at("hub_id");

				nlohmann::json jsonDevicesInfo = JsonFromFile("DevicesInfo.json");
				if (jsonDevicesInfo != NULL)
				{
					jsonDevicesInfo["request_id"] = request_id;
					jsonDevicesInfo["hub_id"] = hub_id;

					std::string response_string = jsonDevicesInfo.dump();

					response.setChunkedTransferEncoding(true);
					response.setContentType("application/json");
					std::ostream &ostr = response.send();
					ostr << response_string;
					response.setStatus(Poco::Net::HTTPServerResponse::HTTP_OK);
				}
			} else
				std::cout << "Not Authorization" << std::endl;
		}
	}
	catch (Poco::Exception &ex)
	{
		std::cerr << "HTTP exception: " << ex.displayText() << ". Code:" << ex.code() << std::endl;
	}
	catch (nlohmann::json::exception &ex)
	{
		std::cerr << "JSON exception: " << ex.what() << ". Id:" << ex.id << std::endl;
	}
	catch (std::ios_base::failure &ex)
	{
		std::cerr << "File exception: " << ex.what() << ". Code:" << ex.code() << std::endl;
	}
}


void Condition::handleRequest(Poco::Net::HTTPServerRequest& request, Poco::Net::HTTPServerResponse& response)
{
    try
    {
	    nlohmann::json jsonClientInfo = JsonFromFile("ClientInfo.json");
	    if(jsonClientInfo != NULL)
	    {
		    if (jsonClientInfo.at("key_authorization") == request.get("Authorization"))
		    {
			    nlohmann::json jsonConditionInfo = JsonFromFile("ConditionDevices.json");
			    if(jsonConditionInfo != NULL)
			    {
				    std::string recv_string;
				    Poco::StreamCopier::copyToString(request.stream(), recv_string);
				    std::cout << recv_string << std::endl;
				    nlohmann::json jsonRecv = nlohmann::json::parse(recv_string);
				    nlohmann::json jsonResponse;

				    nlohmann::json jsonDeviceError;
				    for (auto deviceRecv: jsonRecv.at("devices"))
				    {
						bool deviceFind(false);
					    for (auto deviceCondition: jsonConditionInfo.at("devices"))
					    {
						    if (deviceRecv.at("id") == deviceCondition.at("id"))
						    {
							    jsonResponse["devices"].push_back(deviceCondition);
								deviceFind=true;
							    break;
						    }
					    }
						if(!deviceFind)
						{
							jsonDeviceError["id"] = deviceRecv.at("id");
							jsonDeviceError["error_code"] = "DEVICE_UNREACHABLE";
							jsonDeviceError["error_message"] = "Устройство не отвечает. Проверьте, вдруг оно выключено.";
							jsonResponse["devices"].push_back(jsonDeviceError);
						}
				    }

				    std::string request_id = request.get("X-Request-Id");
				    jsonResponse["request_id"] = request_id;

				    std::string response_string = jsonResponse.dump();

				    response.setChunkedTransferEncoding(true);
				    response.setContentType("application/json");
				    std::ostream &ostr = response.send();
				    ostr << response_string;
				    response.setStatus(Poco::Net::HTTPServerResponse::HTTP_OK);
			    }
		    } else
			    std::cout << "Not Authorization" << std::endl;
	    }
    }
    catch (Poco::Exception &ex)
    {
        std::cerr <<"HTTP exception: "<< ex.displayText()<<". Code:"<<ex.code()<< std::endl;
    }
    catch (nlohmann::json::exception &ex)
    {
        std::cerr <<"JSON exception: "<< ex.what()<<". Id:"<<ex.id << std::endl;
    }
    catch (std::ios_base::failure &ex)
    {
        std::cerr <<"File exception: "<< ex.what()<<". Code:"<<ex.code() << std::endl;
    }
}

void Action::handleRequest(Poco::Net::HTTPServerRequest& request, Poco::Net::HTTPServerResponse& response)
{
	try
	{
		nlohmann::json jsonClientInfo = JsonFromFile("ClientInfo.json");
		if (jsonClientInfo != NULL)
		{
			if (jsonClientInfo.at("key_authorization") == request.get("Authorization"))
			{
				nlohmann::json jsonConditionInfo = JsonFromFile("ConditionDevices.json");
				if(jsonConditionInfo != NULL)
				{
					std::string recv_string;
					Poco::StreamCopier::copyToString(request.stream(), recv_string);
					std::cout << recv_string << std::endl;
					nlohmann::json jsonNewCondition = nlohmann::json::parse(recv_string);

					nlohmann::json jsonConditionStatus;
					jsonConditionStatus["request_id"] = request.get("X-Request-Id");

					nlohmann::json jsonTempDevice, jsonTempCapability, jsonTempState, jsonTempActionResult;
					bool flDeviceFound, flCapabilityInstanceFound;
					for (const auto &itemNewCondition: jsonNewCondition.at("devices"))
					{
						flDeviceFound = false;
						jsonTempDevice["id"] = itemNewCondition.at("id");
						for (auto &itemOldCondition: jsonConditionInfo.at("devices"))
						{
							if (itemNewCondition.at("id") == itemOldCondition.at("id"))
							{
								flDeviceFound = true;
								for (const auto &itemCapabilityNewCondition: itemNewCondition.at("capabilities"))
								{
									flCapabilityInstanceFound = false;
									jsonTempCapability["type"] = itemCapabilityNewCondition.at("type");
									jsonTempState["instance"] = itemCapabilityNewCondition.at("state").at("instance");
									for (auto &itemCapabilityOldCondition: itemOldCondition.at("capabilities"))
									{
										if (itemCapabilityNewCondition.at("type") == itemCapabilityOldCondition.at("type"))
										{
											if (itemCapabilityNewCondition.at("state").at("instance") ==
											    itemCapabilityOldCondition.at("state").at("instance"))
											{
												flCapabilityInstanceFound = true;
												jsonTempActionResult["status"] = "DONE";
												jsonTempState["action_result"] = jsonTempActionResult;
												itemCapabilityOldCondition["state"]["value"] = itemCapabilityNewCondition.at("state").at("value");
											}
										}
									}
									if (!flCapabilityInstanceFound)
									{
										jsonTempActionResult["status"] = "ERROR";
										jsonTempActionResult["error_code"] = "INVALID_ACTION";
										jsonTempActionResult["error_message"] = "the human readable error message";

										jsonTempState["action_result"] = jsonTempActionResult;
									}
									jsonTempCapability["state"] = jsonTempState;
									jsonTempDevice["capabilities"].push_back(jsonTempCapability);
									jsonTempState.clear();
									jsonTempCapability.clear();
									jsonTempActionResult.clear();
								}
							}
						}
						if (!flDeviceFound)
						{
							jsonTempActionResult["status"] = "ERROR";
							jsonTempActionResult["error_code"] = "DEVICE_UNREACHABLE";
							jsonTempActionResult["error_message"] = "the human readable error message";
							jsonTempDevice["action_result"] = jsonTempActionResult;
							jsonTempActionResult.clear();
						}
						jsonConditionStatus["devices"].push_back(jsonTempDevice);
						jsonTempDevice.clear();
					}

					std::ofstream fileNewConditionInfo("ConditionDevices.json");
					fileNewConditionInfo.exceptions(std::ifstream::failbit | std::ifstream::badbit);
					fileNewConditionInfo << std::setw(4) << jsonConditionInfo << std::endl;

					std::string response_string = jsonConditionStatus.dump();
					std::cout << response_string << std::endl;

					response.setChunkedTransferEncoding(true);
					response.setContentType("application/json");
					std::ostream &ostr = response.send();
					ostr << response_string;
					response.setStatus(Poco::Net::HTTPServerResponse::HTTP_OK);
				}
			} else
				std::cout << "Not Authorization" << std::endl;
		}
	}
	catch (Poco::Exception &ex)
	{
		std::cerr << "HTTP exception: " << ex.displayText() << ". Code:" << ex.code() << std::endl;
	}
	catch (nlohmann::json::exception &ex)
	{
		std::cerr << "JSON exception: " << ex.what() << ". Id:" << ex.id << std::endl;
	}
	catch (std::ios_base::failure &ex)
	{
		std::cerr << "File exception: " << ex.what() << ". Code:" << ex.code() << std::endl;
	}
}


void Leave::handleRequest(Poco::Net::HTTPServerRequest& request, Poco::Net::HTTPServerResponse& response)
{
	try
	{
		nlohmann::json jsonClientInfo = JsonFromFile("ClientInfo.json");
		if (jsonClientInfo != NULL)
		{
			if (jsonClientInfo.at("key_authorization") == request.get("Authorization"))
			{
				nlohmann::json jsonTempRequest;
				std::string request_id = request.get("X-Request-Id");
				jsonTempRequest["request_id"] = request_id;

				std::string response_string = jsonTempRequest.dump();

				response.setChunkedTransferEncoding(true);
				response.setContentType("application/json");
				std::ostream &ostr = response.send();
				ostr << response_string;
				response.setStatus(Poco::Net::HTTPServerResponse::HTTP_OK);
			} else
				std::cout << "Not Authorization" << std::endl;
		}
	}
	catch (Poco::Exception &ex)
	{
		std::cerr << "HTTP exception: " << ex.displayText() << ". Code:" << ex.code() << std::endl;
	}
	catch (nlohmann::json::exception &ex)
	{
		std::cerr << "JSON exception: " << ex.what() << ". Id:" << ex.id << std::endl;
	}
	catch (std::ios_base::failure &ex)
	{
		std::cerr << "File exception: " << ex.what() << ". Code:" << ex.code() << std::endl;
	}
}

void PutInfo::handleRequest(Poco::Net::HTTPServerRequest& request, Poco::Net::HTTPServerResponse& response)
{
	try
	{
		std::string recv_string;
		Poco::StreamCopier::copyToString(request.stream(), recv_string);
		std::cout << recv_string << std::endl;
		nlohmann::json jsonRecv = nlohmann::json::parse(recv_string);
		Listener::sender.SetUri(jsonRecv.at("ip"), jsonRecv.at("port"));
		response.setStatus(Poco::Net::HTTPServerResponse::HTTP_OK);
		response.send();
	}
	catch (Poco::Exception &ex)
	{
		std::cerr << "HTTP exception: " << ex.displayText() << ". Code:" << ex.code() << std::endl;
	}
	catch (nlohmann::json::exception &ex)
	{
		std::cerr << "JSON exception: " << ex.what() << ". Id:" << ex.id << std::endl;
	}
}

void GetInfo::handleRequest(Poco::Net::HTTPServerRequest& request, Poco::Net::HTTPServerResponse& response)
{
	try
	{
		nlohmann::json jsonTempRequest;
		jsonTempRequest["hub_id"] = Listener::sender.GetHubId();
		jsonTempRequest["uri"] = Listener::sender.GetUri();
		jsonTempRequest["key"] = Listener::sender.GetKeyAuth();

		std::string response_string = jsonTempRequest.dump();

		response.setChunkedTransferEncoding(true);
		response.setContentType("application/json");
		std::ostream &ostr = response.send();
		ostr << jsonTempRequest;
		response.setStatus(Poco::Net::HTTPServerResponse::HTTP_OK);
	}
	catch (Poco::Exception &ex)
	{
		std::cerr << "HTTP exception: " << ex.displayText() << ". Code:" << ex.code() << std::endl;
	}
	catch (nlohmann::json::exception &ex)
	{
		std::cerr << "JSON exception: " << ex.what() << ". Id:" << ex.id << std::endl;
	}
}

void DoSomething::handleRequest(Poco::Net::HTTPServerRequest &request, Poco::Net::HTTPServerResponse &response)
{
	try
	{
		Poco::URI uri(request.getURI());
		int codeReq = Listener::sender.handlerChosenNumRequest(std::stoi(uri.getQuery()));
		nlohmann::json jsonTempRequest;
		if(codeReq == 0)
			jsonTempRequest["status"] = "Done";
		else
		{
			jsonTempRequest["status"] = "Error";
			jsonTempRequest["error_code"] = codeReq;
			if(codeReq == httpEx)
				jsonTempRequest["error_message"] = "POCO HTTP error";
			else if(codeReq == jsonEx)
				jsonTempRequest["error_message"] = "Nlohmann JSON error";
			else if(codeReq == iosEx)
				jsonTempRequest["error_message"] = "IOS error";
			else jsonTempRequest["error_message"] = "Unknown error";
		}
		response.setChunkedTransferEncoding(true);
		response.setContentType("application/json");
		std::ostream &ostr = response.send();
		ostr << jsonTempRequest;
		response.setStatus(Poco::Net::HTTPServerResponse::HTTP_OK);
	}
	catch (Poco::Exception &ex)
	{
		std::cerr << "HTTP exception: " << ex.displayText() << ". Code:" << ex.code() << std::endl;
	}
}
