// Listener.h
#ifndef SERVER_LISTENER_H_
#define SERVER_LISTENER_H_
 
#include "CommonRequestHandler.h"
#include "Sender.h"


class Listener
{
	uint16_t StartServer(uint16_t port);
public:
	static Sender sender;
	Listener(uint16_t port);
	~Listener();

};
#endif // SERVER_LISTENER_H_