//Sender.cpp
#include "Sender.h"
#include <stdlib.h>
#include <unistd.h>
#include <ext/stdio_filebuf.h>
#include <sys/file.h>

Sender::Sender()
{
	try
	{
		_uri = ("http://localhost:60000");
		nlohmann::json jsonClientInfo = JsonFromFile("ClientInfo.json");
		if (jsonClientInfo != NULL)
		{
			_keyAuth = jsonClientInfo.at("key_authorization");
		}
	}
	catch (Poco::Exception &ex)
	{
		std::cerr << "HTTP exception: " << ex.displayText() << ". Code:" << ex.code() << std::endl;
		//return httpEx;
	}
	catch (nlohmann::json::exception &ex)
	{
		std::cerr << "JSON exception: " << ex.what() << ". Id:" << ex.id << std::endl;
		//return jsonEx;
	}
}

int16_t Sender::handlerChosenNumRequest(const uint16_t& numberRequest) {
    switch (numberRequest) {
	    case 0:
		    return smarthomeTestAlarm();
        case 1:
	        return smarthomeIntroduction();
	    case 2:
		    return smarthomeActionNotification();
	    case 3:
		    return smarthomeParametersNotification();
	    case 4:
		    return smarthomeErrorsNotification();
	    case 5:
		    return smarthomeLeave();
        default:
        {
			std::cout<<"no request under the selected number"<<std::endl;
	        return -1;
		}
    }
}


int16_t Sender::smarthomeIntroduction()
{
	try
	{
		nlohmann::json jsonClientInfo = JsonFromFile("ClientInfo.json");
		if (jsonClientInfo != NULL)
		{
			Poco::Net::HTTPRequest request(Poco::Net::HTTPRequest::HTTP_POST, "/v1.3/smarthome/ext_controller/introduction", Poco::Net::HTTPMessage::HTTP_1_1);

			// get response
			Poco::Net::HTTPResponse res;
			std::string recvString;
			int codeReturnSend = Send(request, jsonClientInfo, res, &recvString);
			if(codeReturnSend == 0)
			{
				std::cout << res.getVersion() << " " << res.getStatus() << " " << res.getReason() << std::endl;
				std::cout << recvString << std::endl;
				nlohmann::json recvJson = nlohmann::json::parse(recvString);

				_hubId = recvJson.at("hub_id");
				jsonClientInfo["hub_id"] = _hubId;

				codeReturnSend = JsonToFile(jsonClientInfo, "ClientInfo.json");
			}
			return codeReturnSend;
		}
	}
	catch (Poco::Exception &ex)
	{
		std::cerr << "HTTP exception: " << ex.displayText() << ". Code:" << ex.code() << std::endl;
		return httpEx;
	}
	catch (nlohmann::json::exception &ex)
	{
		std::cerr << "JSON exception: " << ex.what() << ". Id:" << ex.id << std::endl;
		return jsonEx;
	}
	catch (std::ios_base::failure &ex)
	{
		std::cerr << "File exception: " << ex.what() << ". Code:" << ex.code() << std::endl;
		return iosEx;
	}
}


int16_t Sender::smarthomeTestAlarm()
{
	try
	{
		nlohmann::json jsonClientInfo = JsonFromFile("ClientInfo.json");
		if (jsonClientInfo != NULL)
		{
			_keyAuth = jsonClientInfo.at("key_authorization");
			bool alarm = true;
			for(int i=0;i<2;i++)
			{
				sleep(rand() % 20 + 10);
				std::cout << "sending alarm " << std::endl;
				SendAlarm(alarm);
				std::cout << "alarm sent !" << std::endl;
				alarm = !alarm;
			}
		}
	}
	catch (Poco::Exception &ex)
	{
		std::cerr << "HTTP exception: " << ex.displayText() << ". Code:" << ex.code() << std::endl;
		return httpEx;
	}
	catch (nlohmann::json::exception &ex)
	{
		std::cerr << "JSON exception: " << ex.what() << ". Id:" << ex.id << std::endl;
		return jsonEx;
	}
	catch (std::ios_base::failure &ex)
	{
		std::cerr << "File exception: " << ex.what() << ". Code:" << ex.code() << std::endl;
		return iosEx;
	}
	return 0;
}

int16_t Sender::smarthomeActionNotification()
{
	try
	{
		nlohmann::json jsonActionNotification = JsonFromFile("ActionNotification.json");
		if (jsonActionNotification != NULL)
		{
			jsonActionNotification["hub_id"] = _hubId;

			Poco::Net::HTTPClientSession session(_uri.getHost(), _uri.getPort());
			session.setKeepAlive(true);

			Poco::Net::HTTPRequest request(Poco::Net::HTTPRequest::HTTP_POST, "/v1.3/smarthome/ext_controller/action_notification", Poco::Net::HTTPMessage::HTTP_1_1);

			Poco::Net::HTTPResponse res;
			std::string recvString;
			int codeReturnSend = Send(request, jsonActionNotification, res, std::nullopt);
			if(codeReturnSend == 0)
			{
				std::cout << res.getVersion() << " " << res.getStatus() << " " << res.getReason() << std::endl;
			}
			return codeReturnSend;
		}
	}
	catch (Poco::Exception &ex)
	{
		std::cerr << "HTTP exception: " << ex.displayText() << ". Code:" << ex.code() << std::endl;
		return httpEx;
	}
	catch (nlohmann::json::exception &ex)
	{
		std::cerr << "JSON exception: " << ex.what() << ". Id:" << ex.id << std::endl;
		return jsonEx;
	}
	catch (std::ios_base::failure &ex)
	{
		std::cerr << "File exception: " << ex.what() << ". Code:" << ex.code() << std::endl;
		return iosEx;
	}
}


int16_t Sender::smarthomeParametersNotification()
{
	try
	{
		nlohmann::json jsonClientInfo = JsonFromFile("ClientInfo.json");//TODO: Delete after the client is running non-stop
		nlohmann::json jsonParametersNotification = JsonFromFile("ParametersNotification.json");
		if (jsonClientInfo != NULL && jsonParametersNotification != NULL)
		{
			_hubId = jsonClientInfo.at("hub_id");//TODO: Delete after the client is running non-stop
			jsonParametersNotification["hub_id"] = _hubId;
			std::string stringRequest = jsonParametersNotification.dump();

			Poco::Net::HTTPClientSession session(_uri.getHost(), _uri.getPort());
			session.setKeepAlive(true);

			Poco::Net::HTTPRequest request(Poco::Net::HTTPRequest::HTTP_POST, "/v1.3/smarthome/ext_controller/parameters_notification", Poco::Net::HTTPMessage::HTTP_1_1);
			request.setContentType("application/json");
			request.add("X-Request-Id", "0000001");
			request.add("Authorization", jsonClientInfo.at("key_authorization"));
			request.setContentLength(stringRequest.length());
			session.sendRequest(request) << stringRequest;

			// get response
			Poco::Net::HTTPResponse res;
			std::string recvString;
			std::istream &is = session.receiveResponse(res);
			std::cout << res.getVersion() << " " << res.getStatus() << " " << res.getReason() << std::endl;
			Poco::StreamCopier::copyToString(is, recvString);
			std::cout << recvString << std::endl;
		}
	}
	catch (Poco::Exception &ex)
	{
		std::cerr << "HTTP exception: " << ex.displayText() << ". Code:" << ex.code() << std::endl;
		return httpEx;
	}
	catch (nlohmann::json::exception &ex)
	{
		std::cerr << "JSON exception: " << ex.what() << ". Id:" << ex.id << std::endl;
		return jsonEx;
	}
	catch (std::ios_base::failure &ex)
	{
		std::cerr << "File exception: " << ex.what() << ". Code:" << ex.code() << std::endl;
		return iosEx;
	}
	return 0;
}


int16_t Sender::smarthomeErrorsNotification()
{
	try
	{
		nlohmann::json jsonClientInfo = JsonFromFile("ClientInfo.json");//TODO: Delete after the client is running non-stop
		nlohmann::json jsonErrorsNotification = JsonFromFile("ErrorsNotification.json");
		if (jsonClientInfo != NULL && jsonErrorsNotification != NULL)
		{
			_hubId = jsonClientInfo.at("hub_id");//TODO: Delete after the client is running non-stop
			jsonErrorsNotification["hub_id"] = _hubId;
			std::string stringRequest = jsonErrorsNotification.dump();

			Poco::Net::HTTPClientSession session(_uri.getHost(), _uri.getPort());
			session.setKeepAlive(true);

			Poco::Net::HTTPRequest request(Poco::Net::HTTPRequest::HTTP_POST, "/v1.3/smarthome/ext_controller/errors_notification", Poco::Net::HTTPMessage::HTTP_1_1);
			request.setContentType("application/json");
			request.add("X-Request-Id", "0000001");
			request.add("Authorization", jsonClientInfo.at("key_authorization"));
			request.setContentLength(stringRequest.length());
			session.sendRequest(request) << stringRequest;

			// get response
			Poco::Net::HTTPResponse res;
			std::string recvString;
			std::istream &is = session.receiveResponse(res);
			std::cout << res.getVersion() << " " << res.getStatus() << " " << res.getReason() << std::endl;
			Poco::StreamCopier::copyToString(is, recvString);
			std::cout << recvString << std::endl;
		}
	}
	catch (Poco::Exception &ex)
	{
		std::cerr << "HTTP exception: " << ex.displayText() << ". Code:" << ex.code() << std::endl;
		return httpEx;
	}
	catch (nlohmann::json::exception &ex)
	{
		std::cerr << "JSON exception: " << ex.what() << ". Id:" << ex.id << std::endl;
		return jsonEx;
	}
	catch (std::ios_base::failure &ex)
	{
		std::cerr << "File exception: " << ex.what() << ". Code:" << ex.code() << std::endl;
		return iosEx;
	}
	return 0;
}


int16_t Sender::smarthomeLeave()
{
	try
	{
		nlohmann::json jsonClientInfo = JsonFromFile("ClientInfo.json");//TODO: Delete after the client is running non-stop
		nlohmann::json jsonLeave = JsonFromFile("Leave.json");
		if (jsonClientInfo != NULL && jsonLeave != NULL)
		{
			_hubId = jsonClientInfo.at("hub_id");//TODO: Delete after the client is running non-stop
			jsonLeave["hub_id"] = _hubId;
			std::string stringRequest = jsonLeave.dump();

			Poco::Net::HTTPClientSession session(_uri.getHost(), _uri.getPort());
			session.setKeepAlive(true);

			Poco::Net::HTTPRequest request(Poco::Net::HTTPRequest::HTTP_POST, "/v1.3/smarthome/ext_controller/leave", Poco::Net::HTTPMessage::HTTP_1_1);
			request.setContentType("application/json");
			request.add("X-Request-Id", "0000001");
			request.add("Authorization", jsonClientInfo.at("key_authorization"));
			request.setContentLength(stringRequest.length());
			session.sendRequest(request) << stringRequest;

			// get response
			Poco::Net::HTTPResponse res;
			std::string recvString;
			std::istream &is = session.receiveResponse(res);
			std::cout << res.getVersion() << " " << res.getStatus() << " " << res.getReason() << std::endl;
			Poco::StreamCopier::copyToString(is, recvString);
			std::cout << recvString << std::endl;
		}
	}
	catch (Poco::Exception &ex)
	{
		std::cerr << "HTTP exception: " << ex.displayText() << ". Code:" << ex.code() << std::endl;
		return httpEx;
	}
	catch (nlohmann::json::exception &ex)
	{
		std::cerr << "JSON exception: " << ex.what() << ". Id:" << ex.id << std::endl;
		return jsonEx;
	}
	catch (std::ios_base::failure &ex)
	{
		std::cerr << "File exception: " << ex.what() << ". Code:" << ex.code() << std::endl;
		return iosEx;
	}
	return 0;
}

Sender::~Sender() = default;

int16_t Sender::SendAlarm(const bool& alarm)
{
	nlohmann::json jsonAlarm = JsonFromFile("Alarm.json");
	if(jsonAlarm != NULL)
	{
		jsonAlarm["devices"][0]["properties"][0]["state"]["value"] = alarm ? "open" : "closed";
		jsonAlarm["hub_id"] = _hubId;
		Poco::Net::HTTPRequest request(Poco::Net::HTTPRequest::HTTP_POST, "/v1.3/smarthome/ext_controller/action_notification", Poco::Net::HTTPMessage::HTTP_1_1);
		Poco::Net::HTTPResponse res;
		int codeReturnSend = Send(request, jsonAlarm, res, std::nullopt);
		if (codeReturnSend == 0)
		{
			std::cout << "response: " << std::endl;
			std::cout << res.getVersion() << " " << res.getStatus() << " " << res.getReason() << std::endl;
			return 0;
		}
		else
		{
			std::cout<<"Something went wrong!"<<std::endl;
			return codeReturnSend;
		}
	}
	else return jsonEx;
}

int16_t Sender::Send(Poco::Net::HTTPRequest& req, const nlohmann::json& js, Poco::Net::HTTPResponse& response, std::optional<std::string*> resStr)
{
	Poco::Net::HTTPClientSession session;
	try
	{
		session.setHost(_uri.getHost());
		session.setPort(_uri.getPort());
		session.setKeepAlive(true);
		req.setContentType("application/json");
		req.add("X-Request-Id", "0000001");
		req.add("Authorization", _keyAuth);
		std::string reqStr = js.dump();
		req.setContentLength(reqStr.length());
		session.sendRequest(req) << js;
		std::istream &is = session.receiveResponse(response);
		if(resStr)
		{
			Poco::StreamCopier::copyToString(is, **resStr);
		}
		return 0;
	}
	catch (Poco::Exception &ex)
	{
		std::cerr << "HTTP exception: " << ex.displayText() << ". Code:" << ex.code() << std::endl;
		session.reset();
		return httpEx;
	}
	catch (nlohmann::json::exception &ex)
	{
		std::cerr << "JSON exception: " << ex.what() << ". Id:" << ex.id << std::endl;
		return jsonEx;
	}
	catch (std::ios_base::failure &ex)
	{
		std::cerr << "File exception: " << ex.what() << ". Code:" << ex.code() << std::endl;
		return iosEx;
	}
}

void Sender::SetUri(std::string ip, std::string port)
{
	_uri = ("http://"+ip+":"+port);
}


std::string Sender::GetUri()
{
	return _uri.toString();
}

std::string Sender::GetKeyAuth()
{
	return _keyAuth;
}

uint16_t Sender::GetHubId()
{
	return _hubId;
}


#include <errno.h>//TODO: Delete after all testing
nlohmann::json JsonFromFile(const std::string& fileName)
{
	try
	{
		std::ifstream myFile(fileName);
		int fd = static_cast< __gnu_cxx::stdio_filebuf< char > * const >( myFile.rdbuf() )->fd();
		if(flock(fd, LOCK_SH)!=0)
			std::cout<<"Flock SH:" << errno << std::endl;//TODO: Delete after all testing
		myFile.exceptions(std::ifstream::failbit | std::ifstream::badbit);
		nlohmann::json myJson;
		myFile >> myJson;
		myFile.close();
		return myJson;
	}
	catch (std::ios_base::failure &ex)
	{
		std::cerr << "File" << fileName << " exception: " << ex.what() << ". Code:" << ex.code() << std::endl;
		return NULL;
	}
	catch (nlohmann::json::exception &ex)
	{
		std::cerr << "JSON exception: " << ex.what() << ". Id:" << ex.id << std::endl;
		return NULL;
	}
}


int16_t JsonToFile(nlohmann::json myJson, const std::string &fileName)
{
	try
	{
		std::ofstream myFile(fileName);
		int fd = static_cast< __gnu_cxx::stdio_filebuf< char > * const >( myFile.rdbuf() )->fd();
		if(flock(fd, LOCK_EX)!=0)
			std::cout<<"Flock EX:" << errno << std::endl;//TODO: Delete after all testing
		myFile.exceptions(std::ifstream::failbit | std::ifstream::badbit);
		myFile << std::setw(4) << myJson << std::endl;
		return 0;
	}
	catch (nlohmann::json::exception &ex)
	{
		std::cerr << "JSON exception: " << ex.what() << ". Id:" << ex.id << std::endl;
		return jsonEx;
	}
	catch (std::ios_base::failure &ex)
	{
		std::cerr << "File" << fileName << " exception: " << ex.what() << ". Code:" << ex.code() << std::endl;
		return iosEx;
	}
}
