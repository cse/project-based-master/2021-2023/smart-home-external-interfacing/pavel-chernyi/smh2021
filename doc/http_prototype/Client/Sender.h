#ifndef CLIENT_SENDER_H_
#define CLIENT_SENDER_H_
 
#include <Poco/Util/Application.h>
#include <Poco/Net/HTTPClientSession.h>
#include <Poco/Net/HTTPRequest.h>
#include <Poco/Net/HTTPResponse.h>
#include <Poco/StreamCopier.h>
#include <Poco/Path.h>
#include <Poco/URI.h>
#include <Poco/Exception.h>
#include <iostream>
#include <string>
#include <nlohmann/json.hpp>
#include <fstream>
#include <iomanip>
#include <optional>

#define httpEx  -10
#define jsonEx  -11
#define iosEx   -12

nlohmann::json JsonFromFile(const std::string& fileName);
int16_t JsonToFile(nlohmann::json myJson, const std::string& fileName);

class Sender
{
    Poco::URI 	_uri;
	std::string _keyAuth;
	uint16_t 	_hubId;

    int16_t smarthomeIntroduction();
	int16_t smarthomeActionNotification();
	int16_t smarthomeParametersNotification();
	int16_t smarthomeErrorsNotification();
	int16_t smarthomeLeave();
	int16_t smarthomeTestAlarm();
	int16_t SendAlarm(const bool& alarm);
	int16_t Send(Poco::Net::HTTPRequest& req, const nlohmann::json& js, Poco::Net::HTTPResponse& response, std::optional<std::string*> resStr);
public:
	Sender();
    ~Sender();
	int16_t handlerChosenNumRequest(const uint16_t& numberRequest);
	void SetUri(std::string ip, std::string port);
	std::string GetUri();
	std::string GetKeyAuth();
	uint16_t GetHubId();

};

#endif // CLIENT_SENDER_H_