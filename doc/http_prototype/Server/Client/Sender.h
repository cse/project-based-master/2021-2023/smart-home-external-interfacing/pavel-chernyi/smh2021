// Sender.h
#ifndef SERVER_SENDER_H_
#define SERVER_SENDER_H_
 
#include <Poco/Util/Application.h>
#include <Poco/Net/HTTPClientSession.h>
#include <Poco/Net/HTTPRequest.h>
#include <Poco/Net/HTTPResponse.h>
#include <Poco/StreamCopier.h>
#include <Poco/Path.h>
#include <Poco/URI.h>
#include <Poco/Exception.h>
#include <iostream>
#include <string>
#include <nlohmann/json.hpp>
#include <fstream>
#include <iomanip> 


class Sender : public Poco::Util::Application
{
 virtual int main(const std::vector<std::string>& args) 
 {
 try
  {
    Poco::URI uri("http://"+args[1]);
    uint16_t requestNumber = std::stoi(args[0]);

    if(requestNumber==1)//GetDevices
    {
        Poco::Net::HTTPClientSession session(uri.getHost(), uri.getPort());
        session.setKeepAlive(true);

        Poco::Net::HTTPRequest request(Poco::Net::HTTPRequest::HTTP_GET, "/devices", Poco::Net::HTTPMessage::HTTP_1_1);
        request.add("X-Request-Id", "0000001");
        session.sendRequest(request);

        // get response
        Poco::Net::HTTPResponse res;
        std::string recvString;
        std::istream &is = session.receiveResponse(res);
        std::cout << res.getVersion() << " " << res.getStatus() << " " << res.getReason() << std::endl;
        Poco::StreamCopier::copyToString(is, recvString);
        std::cout << recvString << std::endl;

        nlohmann::json recvJson = nlohmann::json::parse(recvString);
        std::ifstream fileInServerDevicesInfo("serverDevicesInfo.json");
        fileInServerDevicesInfo.exceptions(std::ifstream::failbit | std::ifstream::badbit);
        nlohmann::json jsonServerDevicesInfo;
        fileInServerDevicesInfo >> jsonServerDevicesInfo;

        recvJson.erase("request_id");////For logging need save somewhere before erase

        bool flUserReWritten = false;
        if(jsonServerDevicesInfo.contains("payload")) {
            for (auto &payloadServerDevicesInfo: jsonServerDevicesInfo.at("payload")) {
                if (payloadServerDevicesInfo.at("hub_id") == recvJson.at("hub_id")) {
                    payloadServerDevicesInfo = recvJson;
                    flUserReWritten = true;
                    break;
                }
            }
        }
        if (!flUserReWritten)
            jsonServerDevicesInfo["payload"] += recvJson;
        std::ofstream fileOutDeviceInfo("serverDevicesInfo.json");
        fileOutDeviceInfo.exceptions(std::ifstream::failbit | std::ifstream::badbit);
        fileOutDeviceInfo << std::setw(4) << jsonServerDevicesInfo << std::endl;
    }
    else if(requestNumber==2)//PostCondition
    {
        Poco::Net::HTTPClientSession session(uri.getHost(), uri.getPort());
        session.setKeepAlive(true);

        std::string IP = uri.getHost();

        std::ifstream fileClientsInfo("ClientsInfo.json");
        fileClientsInfo.exceptions(std::ifstream::failbit | std::ifstream::badbit);
        nlohmann::json jsonClientsInfo;
        fileClientsInfo >> jsonClientsInfo;

        std::string hub_id;
        for (const auto &payloadClientsInfo: jsonClientsInfo.at("payload")) {
            if (payloadClientsInfo.at("IP") == IP) {
                hub_id = payloadClientsInfo.at("hub_id");
                break;
            }
        }

        nlohmann::json jsonRequest;
        nlohmann::json tempJsonRequest;
        std::ifstream fileDevicesInfo("serverDevicesInfo.json");
        fileDevicesInfo.exceptions(std::ifstream::failbit | std::ifstream::badbit);
        nlohmann::json jsonDevicesInfo;
        fileDevicesInfo >> jsonDevicesInfo;

        for (const auto &payloadDevicesInfo: jsonDevicesInfo["payload"]) {
            if (payloadDevicesInfo.at("hub_id") == hub_id) {
                for (const auto &deviceDevicesInfo: payloadDevicesInfo.at("devices")) {
                    if (deviceDevicesInfo.contains("custom_data")) {
                        tempJsonRequest["id"] = deviceDevicesInfo.at("id");
                        tempJsonRequest["custom_data"] = deviceDevicesInfo.at("custom_data");
                        jsonRequest["devices"] += tempJsonRequest;
                    } else
                        jsonRequest["request"] += deviceDevicesInfo.at("id");
                }
                break;
            }
        }

        std::string stringConditionInfo = jsonRequest.dump();

        Poco::Net::HTTPRequest request(Poco::Net::HTTPRequest::HTTP_POST, "/condition",
                                       Poco::Net::HTTPMessage::HTTP_1_1);
        request.setContentType("application/json");
        request.add("X-Request-Id", "0000001");
        request.setContentLength(stringConditionInfo.length());
        session.sendRequest(request) << stringConditionInfo;

        // get response
        Poco::Net::HTTPResponse res;
        std::string recvString;
        std::istream &is = session.receiveResponse(res);
        std::cout << res.getVersion() << " " << res.getStatus() << " " << res.getReason() << std::endl;
        Poco::StreamCopier::copyToString(is, recvString);
        std::cout << recvString << std::endl;

        nlohmann::json recvJson = nlohmann::json::parse(recvString);
        std::ifstream fileInConditionInfo("serverConditionDevices.json");
        fileInConditionInfo.exceptions(std::ifstream::failbit | std::ifstream::badbit);
        nlohmann::json jsonInConditionInfo;
        fileInConditionInfo >> jsonInConditionInfo;

        recvJson.erase("request_id");////For logging need save somewhere before erase
        recvJson["hub_id"] = hub_id;

        bool flUserReWritten = false;
        if(jsonInConditionInfo.contains("payload")) {
            for (auto &payloadConditionInfo: jsonInConditionInfo.at("payload")) {
                if (payloadConditionInfo.at("hub_id") == recvJson.at("hub_id")) {
                    payloadConditionInfo = recvJson;
                    flUserReWritten = true;
                    break;
                }
            }
        }
        if (!flUserReWritten)
            jsonInConditionInfo["payload"] += recvJson;
        std::ofstream fileOutConditionDevices("serverConditionDevices.json");
        fileOutConditionDevices.exceptions(std::ifstream::failbit | std::ifstream::badbit);
        fileOutConditionDevices << std::setw(4) << jsonInConditionInfo << std::endl;
    }
    else if(requestNumber==3)//PostNewCondition
    {
        Poco::Net::HTTPClientSession session(uri.getHost(), uri.getPort());
        session.setKeepAlive(true);

        //std::string IP = uri.getHost();

        std::ifstream fileNewCondition("NewCondition.json");
        fileNewCondition.exceptions ( std::ifstream::failbit | std::ifstream::badbit );
        nlohmann::json jsonNewCondition;
        fileNewCondition>>jsonNewCondition;

        std::string stringNewCondition = jsonNewCondition.dump();

        Poco::Net::HTTPRequest request(Poco::Net::HTTPRequest::HTTP_POST, "/action", Poco::Net::HTTPMessage::HTTP_1_1);
        request.setContentType("application/json");
        request.add("X-Request-Id", "0000001");
        request.setContentLength(stringNewCondition.length());
        session.sendRequest(request)<<stringNewCondition;

        // get response
        Poco::Net::HTTPResponse res;
        std::string recvString;
        std::istream & is = session.receiveResponse(res);
        std::cout << res.getVersion()<< " " << res.getStatus() << " " << res.getReason() << std::endl;
        Poco::StreamCopier::copyToString(is, recvString);
        std::cout << recvString << std::endl;

        nlohmann::json recvJson=nlohmann::json::parse(recvString);
        std::ofstream fileStatusChangeCondition("fileStatusChangeCondition.json");
        fileStatusChangeCondition.exceptions ( std::ifstream::failbit | std::ifstream::badbit );
        fileStatusChangeCondition << std::setw(4) << recvJson << std::endl;

        if(res.getReason()=="OK")
        {
            std::string callClient="./serverMainClient 2 "+uri.getHost()+":6666";
            std::system(callClient.c_str());
        }

    }
  }
 catch (Poco::Exception &ex)
 {
     std::cerr <<"HTTP exception: "<< ex.displayText()<<". Code:"<<ex.code()<< std::endl;
     return -1;
 }
 catch (nlohmann::json::exception &ex)
 {
     std::cerr <<"JSON exception: "<< ex.what()<<". Id:"<<ex.id << std::endl;
     return -1;
 }
 catch (std::ios_base::failure &ex)
 {
     std::cerr <<"File exception: "<< ex.what()<<". Code:"<<ex.code() << std::endl;
     return -1;
 }

  return 0;
 }
};
#endif // SERVER_SENDER_H_