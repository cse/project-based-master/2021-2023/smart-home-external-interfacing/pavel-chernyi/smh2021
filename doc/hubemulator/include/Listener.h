#ifndef LISTENER_H_
#define LISTENER_H_
 
#include "CommonRequestHandler.h"
#include "Sender.h"


class Listener
{
	uint16_t StartServer(uint16_t port);
public:
	static Sender sender;
	Listener(uint16_t port);
	~Listener();

};
#endif