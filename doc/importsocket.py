import socket  
  
s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM) 
s.bind(('', 1928)) 
s.settimeout(10)
multicase_address = ('239.255.255.250', 1982) 
msg = "M-SEARCH * HTTP/1.1\r\n" 
msg = msg + "HOST: 239.255.255.250:1982\r\n"
msg = msg + "MAN: \"ssdp:discover\"\r\n"
msg = msg + "ST: wifi_bulb"
s.sendto(msg, multicase_address)

while True:  
    try:
        data, addr = s.recvfrom(1928)
    except socket.timeout:
        break
    print "[+] %s\n%s" % (addr, data)
